<div class="row">
  	<div class="col-md-12">
		@include('admin.partials._subscription_notice')

	    <!-- Error Message -->
		@if (Session::has('error'))
	    	<div class="alert alert-danger">{{ Session::get('error') }}</div>
    	@endif
  	</div>
  	<div class="col-md-8 col-md-offset-2">
		@if(Auth::user()->hasExpiredPlan())
			<div class="alert alert-danger">
				<strong><i class="icon fa fa-info-circle"></i>{{ trans('app.notice') }}</strong>
				{{ trans('messages.subscription_expired') }}
			</div>
		@endif

		@if(Auth::user()->isSubscribed())
			@if($current_plan && ! Auth::user()->isOnGracePeriod())
		  		<div class="panel panel-default">
			  		<div class="panel-body">
						{!! trans('messages.current_subscription', ['plan' => $current_plan->name]) !!}
						@if(Auth::user()->isMerchant())
							{!! Form::open(['route' => ['admin.account.subscription.cancel', $current_plan], 'method' => 'delete', 'class' => 'inline']) !!}
								{!! Form::button(trans('app.calcel_plan'), ['type' => 'submit', 'class' => 'confirm ajax-silent btn btn-sm btn-danger pull-right']) !!}
							{!! Form::close() !!}
				  		@endif
			  		</div>
		  		</div>
	  		@endif
  		@else
			<div class="alert alert-info">
				<strong><i class="icon fa fa-rocket"></i></strong>
				{{ trans('messages.choose_subscription') }}
			</div>
  		@endif

		@unless(Auth::user()->hasBillingToken())
			<div class="alert alert-info">
				<strong><i class="icon fa fa-credit-card"></i></strong>
				{{ trans('messages.no_billing_info') }}
			</div>
  		@endunless

  		<div class="panel panel-default">
	  		<div class="panel-body">
				<fieldset>
					<legend>{{ trans('app.subscription_plans') }}</legend>
			  		<table class="table no-border">
			  			<thead></thead>
			  			<tbody>
					  		@foreach($plans as $plan)
					  			<tr>
					  				<td class="lead">
					  					{{ $plan->name }}
					  				</td>
					  				<td>
			                            <a href="#" data-link="{{ route('admin.account.subscription.features', $plan->plan_id) }}" class="ajax-modal-btn btn btn-default">
			                                <i class="fa fa-star-o"></i> {{ trans('app.features') }}
			                            </a>
					  				</td>
					  				<td class="lead">
					  					<span class="indent20">{{ get_formated_currency($plan->cost) . trans('app.per_month') }}</span>
					  				</td>
									@if(Auth::user()->isMerchant())
						  				<td class="pull-right"> 
                                            <form>
                                              <script src="https://checkout.flutterwave.com/v3.js"></script>

                                              <button type="button" onclick='makePayment("{{ $plan->plan_id . "\"," . $plan->cost }})'> {{ trans('app.select_this_plan') }}</button>


                                             {{--  <a id="reply" onclick='makePayment("{{ $plan->plan_id . "\"," . $plan->cost }})' role="button">Reply</a> --}}
                                            </form>
                                            

				                          {{--  <a href="{{ route('admin.account.subscribe', $plan->plan_id) }}" class="confirm btn btn-lg btn-default">
					                            	<i class="fa fa-leaf"></i> {{ trans('app.select_this_plan') }}
					                         </a> --}}

						  				</td>
					  				@endif
					  			</tr>
					  		@endforeach
			  			</tbody>
			  		</table>

					@if((bool) config('system_settings.trial_days'))
						<span class="spacer10"></span>
						<span class="text-info">
							<strong><i class="icon fa fa-info-circle"></i></strong>
							{{ trans('messages.plan_comes_with_trial',['days' => config('system_settings.trial_days')]) }}
						</span>
					@endif
			  	</fieldset>
			</div>
		</div>

		@if(Auth::user()->isMerchant())
	  		<div class="panel panel-default">
		  		<div class="panel-body">


<script>
	// Bearer FLWPUBK_TEST-1c797580b156ea18fba9db4ed5af997e-X
	// public_key: "FLWPUBK-d114fa66e74b45a093e7e101956187fa-X",
	// public_key: "FLWPUBK_TEST-1c797580b156ea18fba9db4ed5af997e-X",


			let transaction_id = 'mimi-merch-{{Auth::user()->id}}{{\Carbon\Carbon::parse(Auth::user()->created_at)->timestamp}}';

	function makePayment(plan,cost) {
		FlutterwaveCheckout({
		  public_key: "FLWPUBK-d114fa66e74b45a093e7e101956187fa-X",
		  tx_ref: transaction_id,
		  amount: cost,
		  currency: "GHS",
		  payment_options: "mobilemoneyghana",
		  redirect_url: // specified redirect URL
			'https://www.mimi.africa/admin/account/subscribe/'+plan + '?status=paid&transaction_id='+transaction_id,
		  customer: {
			email: '{{Auth::user()->email}}',
			phone_number: '',
			name: '{{Auth::user()->name}}',
		  },
		  callback: function (data) {
			console.log(data);
		  },
		  onclose: function() {
			// close modal
		  },
		  customizations: {
			title: "Mimi Services",
			description: "Payment for " + plan +" Plan Subscription fee",
			logo: "https://placehold.it/100x100/eee?text=Mimi GH",
		  },
		});
  }
</script>
		</div>
			</div>
		@else
			<div class="alert alert-danger">
				<strong><i class="icon fa fa-info-circle"></i>{{ trans('app.notice') }}</strong>
				{{ trans('messages.only_merchant_can_change_plan') }}
			</div>
		@endif

		<fieldset>
			<legend>{{ trans('app.history') }} <i class="fa fa-history"></i> </legend>
			@include('admin.account._activity_logs', ['logger' => Auth::user()->shop])
		</fieldset>
  	</div>
</div>
