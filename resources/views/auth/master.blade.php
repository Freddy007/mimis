<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ $title ?? get_site_title() }}</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <link href="{{ '/public/css/app.css' }}" rel="stylesheet">
     <link href="{{ asset("css/app.css") }}" rel="stylesheet">
    <link href="{{ '/public/css/vendor.css' }}" rel="stylesheet">
     <link href="{{ asset("css/vendor.css") }}" rel="stylesheet">

    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" type="text/css" rel="stylesheet">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
      .select2-selection__arrow{
        display: none;
      }
      .form-control-feedback{
        width: 46px;
        height: 46px;
        line-height: 46px;
      }
      .select2-container--default .select2-selection--single {
          height: 46px !important;
          padding: 10px 16px;
          font-size: 18px;
          line-height: 1.33;
      }
      .select2-container--default .select2-selection--single .select2-selection__rendered {
          line-height: 31px !important;
      }

      .login-box-body{
        background-image: "{{url("/public/images/register.jpg")}}" !important;
      }

      {{--.login-page, .register-page {--}}
      {{--  /*background: #232b2b;*/--}}
      {{--  background: "{{url("/public/images/register.jpg")}}" !important;--}}
      {{--}--}}
    </style>



  </head>
  <body class="hold-transition login-page">
    <div class="login-box">
      @if (count($errors) > 0)
          <div class="alert alert-danger">
              <strong>{{ trans('app.error') }}!</strong> {{ trans('messages.input_error') }}<br><br>
              <ul class="list-group">
                  @foreach ($errors->all() as $error)
                      <li class="list-group-item list-group-item-danger">{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
      @endif

      @if (Session::has('message'))
          <div class="alert alert-success">
            {{ Session::get('message') }}
          </div>
      @endif

      <div class="login-logo">
        <a href="{{ url('/') }}">{{ get_site_title() }}</a>
      </div>

      @yield('content')

    </div>
    <!-- /.login-box -->


    {{-- <script src="{{ mix("js/app.js") }}"></script> --}}
    <script src="{{ asset("public/js/app.js") }}"></script>
    <script src="{{ asset("public/js/vendor.js") }}"></script>
     <script src="{{ asset("js/vendor.js") }}"></script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCrdcQhnH0CYq5VttxFVqT4ssjnIX1EKtI&libraries=places&callback=initAutocomplete"
            async defer></script>

    <!-- Scripts -->
    @yield('scripts', '')

    <script type="text/javascript">
      // ;(function($, window, document) {
        $("#plans").select2({
          minimumResultsForSearch: -1,
        });
        $("#exp-year").select2({
          placeholder: "{{ trans('app.placeholder.exp_year') }}",
          minimumResultsForSearch: -1,
        });
        $("#exp-month").select2({
          placeholder: "{{ trans('app.placeholder.exp_month') }}",
          minimumResultsForSearch: -1,
        });

        $('.icheck').iCheck({
          checkboxClass: 'icheckbox_minimal-blue',
          radioClass: 'iradio_minimal-blue'
        });
      // });
    </script>

    <div class="loader">
      <center>
        <img class="loading-image" src="{{ asset('public/images/gears.gif') }}" alt="busy...">
      </center>
    </div>
  </body>
</html>