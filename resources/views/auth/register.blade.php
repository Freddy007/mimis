@extends('auth.master')

@section('content')
  <div class="box login-box-body">
    <div class="box-header with-border">
      <h3 class="box-title">{{ trans('app.form.register') }}</h3>
    </div> <!-- /.box-header -->
    <div class="box-body">
      {!! Form::open(['route' => 'register', 'id' => config('system_settings.required_card_upfront') ? 'stripe-form' : 'registration-form', 'data-toggle' => 'validator']) !!}
        <div class="form-group has-feedback">
          {{ Form::select('plan', $plans, isset($plan) ? $plan : Null, ['id' => 'plans' , 'class' => 'form-control input-lg', 'required']) }}
            <i class="glyphicon glyphicon-dashboard form-control-feedback"></i>
            <div class="help-block with-errors">
              @if((bool) config('system_settings.trial_days'))
                {{ trans('help.charge_after_trial_days', ['days' => config('system_settings.trial_days')]) }}
              @endif
            </div>
        </div>
        <div class="form-group has-feedback">
          {!! Form::email('email', null, ['class' => 'form-control input-lg', 'placeholder' => trans('app.placeholder.valid_email'), 'required']) !!}
          <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          <div class="help-block with-errors"></div>
        </div>
        <div class="form-group has-feedback">
            {!! Form::password('password', ['class' => 'form-control input-lg', 'id' => 'password', 'placeholder' => trans('app.placeholder.password'), 'data-minlength' => '6', 'required']) !!}
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            <div class="help-block with-errors"></div>
        </div>
        <div class="form-group has-feedback">
            {!! Form::password('password_confirmation', ['class' => 'form-control input-lg', 'placeholder' => trans('app.placeholder.confirm_password'), 'data-match' => '#password', 'required']) !!}
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            <div class="help-block with-errors"></div>
        </div>

        <div class="form-group has-feedback">
            {!! Form::text('shop_name', null, ['class' => 'form-control input-lg', 'placeholder' => trans('app.placeholder.shop_name'), 'required']) !!}
            <i class="fa fa-building form-control-feedback"></i>
            <div class="help-block with-errors"></div>
        </div>

        <div class="form-group has-feedback">
          {!! Form::text('description', null, ['class' => 'form-control input-lg', 'placeholder' => trans('app.placeholder.description'), 'required']) !!}
          <i class="fa fa-sort-desc form-control-feedback"></i>
          <div class="help-block with-errors"></div>
      </div>

        <div class="form-group has-feedback">
          {!! Form::text('phone', null, ['class' => 'form-control input-lg', 'placeholder' => trans('app.placeholder.phone_number'), 'required']) !!}
          <i class="fa fa-phone form-control-feedback"></i>
          <div class="help-block with-errors"></div>
        </div>
        
        <div class="form-group has-feedback">
          {!! Form::text('external_url', null, ['class' => 'form-control input-lg', 'placeholder' => "Website"]) !!}
          <i class="fa fa-info form-control-feedback"></i>
          <div class="help-block with-errors"></div>
        </div>

        <div class="form-group has-feedback">
          
            {!! Form::select('shop_type', [
                'sell-products' => 'Sell products',
                'provide-service' => 'Provide services',
                'mall-supermarket' => 'Mall/Supermarket','Other' => 'other'], null,
                ['class' => 'form-control input-lg','placeholder' => "Nature of business", 'required']) !!}

            <i class="glyphicon glyphicon-equalizer form-control-feedback"></i>
            <div class="help-block with-errors"></div>
        </div>

        <div class="form-group" >
              <div class="row" id="your_location_form">
              <div class="col-md-6">
                <p>Use your current location for shop</p>
              </div>
              <div class="col-md-6">   
              {!! Form::checkbox('name', 'value', true, ["id" => "your_location_checkbox"]) !!}
             </div>
           </div>
      
          <div class="row">
              <p id="demo" style="margin-left: 20px;"></p>
        </div>

        <div class="form-group has-feedback" id="address_box" style="display: none">
            {!! Form::text('address', null, ["onFocus"=>"geolocate()",'class' => 'form-control input-lg', 'placeholder' => "Address","id"=>"autocomplete"]) !!}
            <i class="glyphicon glyphicon-equalizer form-control-feedback"></i>
            <div class="help-block with-errors"></div>
        </div>

        {!! Form::hidden('lat', null, ["id" =>"lat"]) !!}

        {!! Form::hidden('lng', null, ["id" =>"lng"]) !!}

        {!! Form::hidden('address_line_1', null, ["id" =>"address_line_1"]) !!}

        {!! Form::hidden('google_place_id', null, ["id" =>"place_id"]) !!}

        {!! Form::hidden('place_identifier', null, ["id" =>"place_details"]) !!}

    @if(config('system_settings.required_card_upfront'))
{{--          @include('auth.stripe_form')--}}
        @endif

        <div class="row">
          <div class="col-xs-8">
            <div class="form-group">
                <label>
                    {!! Form::checkbox('agree', null, null, ['class' => 'icheck', 'required']) !!} {!! trans('app.form.i_agree_with_merchant_terms', ['url' => route('page.open', \App\Page::PAGE_TNC_FOR_MERCHANT)]) !!}
                </label>
                <div class="help-block with-errors"></div>
            </div>
          </div>

          <div class="col-xs-4">
            {!! Form::submit(trans('app.form.register'), ['class' => 'btn btn-block btn-lg btn-flat btn-primary']) !!}
          </div>
        </div>
      {!! Form::close() !!}

      <a href="{{ route('login') }}" class="btn btn-link">{{ trans('app.form.merchant_login') }}</a>
    </div>

  </div>
  <!-- /.form-box -->


@endsection

@section('scripts')
{{--  @include('plugins.stripe-scripts')--}}

  <script>

      getLocation();

      var placeSearch, autocomplete;

      var componentForm = {
          street_number: 'short_name',
          route: 'long_name',
          locality: 'long_name',
          administrative_area_level_1: 'short_name',
          country: 'long_name',
          postal_code: 'short_name'
      };

      function initAutocomplete() {
          // Create the autocomplete object, restricting the search predictions to
          // geographical location types.
          autocomplete = new google.maps.places.Autocomplete(
              document.getElementById('autocomplete'), {types: ['geocode']});

          // Avoid paying for data that you don't need by restricting the set of
          // place fields that are returned to just the address components.
          autocomplete.setFields(['address_component']);

          // When the user selects an address from the drop-down, populate the
          // address fields in the form.
          // autocomplete.addListener('place_changed', fillInAddress);
          autocomplete.addListener('place_changed', populateCoordinates);
      }

      function fillInAddress() {
          // Get the place details from the autocomplete object.
          var place = autocomplete.getPlace();

          console.log("Place" + JSON.stringify(place));

          for (var component in componentForm) {
              document.getElementById(component).value = '';
              document.getElementById(component).disabled = false;
          }

          // Get each component of the address from the place details,
          // and then fill-in the corresponding field on the form.
          for (var i = 0; i < place.address_components.length; i++) {
              var addressType = place.address_components[i].types[0];
              if (componentForm[addressType]) {
                  var val = place.address_components[i][componentForm[addressType]];
                  document.getElementById(addressType).value = val;
              }
          }
      }

      function populateCoordinates() {

          var addressVal = $("#autocomplete").val();
          let url = "https://maps.googleapis.com/maps/api/geocode/json?address=" + addressVal +"&key=AIzaSyCrdcQhnH0CYq5VttxFVqT4ssjnIX1EKtI";
          $.get( url, function( data ) {
              console.log(JSON.stringify(data.results[0]["geometry"]["location"]["lat"]));
              console.log(JSON.stringify(data.results[0]["geometry"]["location"]["lng"]));

              $("#lat").val(JSON.stringify(data.results[0]["geometry"]["location"]["lat"]));
              $("#lng").val(JSON.stringify(data.results[0]["geometry"]["location"]["lng"]));
              $("#place_id").val(JSON.stringify(data.results[0]["place_id"]));

               var place_details_one = JSON.stringify(data.results[0]["address_components"][0]["long_name"]);
               var place_details_two = JSON.stringify(data.results[0]["address_components"][1]["long_name"]);
               var place_details_three = typeof data.results[0]["address_components"][2] != "undefined" ? JSON.stringify(data.results[0]["address_components"][2]["long_name"]) : "";
               var place_details_four = typeof data.results[0]["address_components"][3] != "undefined" ? JSON.stringify(data.results[0]["address_components"][3]["long_name"]): "";

               $("#place_details").val(" " + place_details_one + ", " + place_details_two + ", " + place_details_three + ", " + place_details_four);

               $("#address_line_1").val(addressVal);
          });
      }

      // Bias the autocomplete object to the user's geographical location,
      // as supplied by the browser's 'navigator.geolocation' object.
      function geolocate() {
          if (navigator.geolocation) {
              navigator.geolocation.getCurrentPosition(function(position) {
                  var geolocation = {
                      // lat: position.coords.latitude,
                      lat: 7.9527706,
                      lng:  -1.0307118
                      // lng: position.coords.longitude
                  };
                  var circle = new google.maps.Circle(
                      {center: geolocation, radius: position.coords.accuracy});
                  autocomplete.setBounds(circle.getBounds());
              });
          }
      }

    var x = document.getElementById("demo");
  

    function getLocation() {
      options = {
	enableHighAccuracy: false,
	timeout:            30000,  // milliseconds (30 seconds)
	maximumAge:         600000 // milliseconds (10 minutes)
}
// navigator.geolocation.getCurrentPosition(handlePosition, handleError, options);

      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition, showError, options);
      } else { 
        x.innerHTML = "Geolocation is not supported by this browser.";
      }
    }

    function showPosition(position) {
      // x.innerHTML = "Latitude: " + position.coords.latitude + 
      // "<br>Longitude: " + position.coords.longitude;

      $("#lat").val( position.coords.latitude);
      $("#lng").val(position.coords.longitude);


    }

    function showError(error) {
      switch(error.code) {
        case error.PERMISSION_DENIED:
          x.innerHTML = "User denied the request for Geolocation."
          $("#your_location_form").hide();
          $("#address_box").show();
          break;
        case error.POSITION_UNAVAILABLE:
          x.innerHTML = "Location information is unavailable."
          $("#your_location_form").hide();
          $("#address_box").show();
          break;
        case error.TIMEOUT:
          x.innerHTML = "The request to get user location timed out."
          $("#your_location_form").hide();
          $("#address_box").show();
          break;
        case error.UNKNOWN_ERROR:
          x.innerHTML = "An unknown error occurred."
          $("#your_location_form").hide();
          $("#address_box").show();
          break;
      }
    }


    $("#your_location_checkbox").click(function(){
      if(!($(this).is(":checked"))){
       console.log("Checkbox is unchecked." );
      //  alert("Checkbox is checked.")
      $("#address_box").show();
  }else{
      $("#address_box").hide();
  }
});

 </script>

@endsection
