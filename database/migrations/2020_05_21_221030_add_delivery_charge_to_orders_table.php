<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDeliveryChargeToOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {

            $table->bigInteger('order_batch_id')
                ->after("order_number")->nullable();

            $table->Decimal('delivery_charge')
                ->after("quantity")->nullable();

            $table->Decimal('total_with_delivery_charge')
                ->after("total")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn("order_batch_id");
            $table->dropColumn("delivery_charge");
            $table->dropColumn("total_with_delivery_charge");

        });
    }
}
