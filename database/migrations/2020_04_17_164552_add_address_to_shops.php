<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAddressToShops extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shops', function (Blueprint $table) {
            $table->string('address')
                ->after("shop_type")->nullable();

            $table->string('town')
                ->after("address")->nullable();

            $table->string('city')
                ->after("town")->nullable();

            $table->string('lat')
                ->after("city")->nullable();

            $table->string('lng')
                ->after("lat")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shops', function (Blueprint $table) {
            $table->dropColumn("address");
            $table->dropColumn("town");
            $table->dropColumn("city");
            $table->dropColumn("lat");
            $table->dropColumn("lng");
        });
    }
}
