<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\ConfigPaypalExpress
 *
 * @property int $shop_id
 * @property string|null $account
 * @property string|null $client_id
 * @property string|null $secret
 * @property bool|null $sandbox
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ConfigPaypalExpress newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ConfigPaypalExpress newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ConfigPaypalExpress query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ConfigPaypalExpress whereAccount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ConfigPaypalExpress whereClientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ConfigPaypalExpress whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ConfigPaypalExpress whereSandbox($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ConfigPaypalExpress whereSecret($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ConfigPaypalExpress whereShopId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ConfigPaypalExpress whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ConfigPaypalExpress extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'config_paypal_express';

    /**
     * The database primary key used by the model.
     *
     * @var string
     */
    protected $primaryKey = 'shop_id';

    /**
     * The attributes that should be casted to boolean types.
     *
     * @var array
     */
    protected $casts = [
        'sandbox' => 'boolean',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
                        'shop_id',
                        'account',
                        'client_id',
                        'secret',
                        'sandbox',
					];

    /**
     * Setters.
     */
    public function setAccountAttribute($value)
    {
        $this->attributes['account'] = trim($value);
    }
    public function setClientIdAttribute($value)
    {
        $this->attributes['client_id'] = trim($value);
    }
    public function setSecretAttribute($value)
    {
        $this->attributes['secret'] = trim($value);
    }
    public function setSandboxAttribute($value)
    {
        $this->attributes['sandbox'] = (bool) $value;
    }

}
