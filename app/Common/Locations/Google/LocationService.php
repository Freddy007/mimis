<?php
/**
 * Created by IntelliJ IDEA.
 * User: Freddy
 * Date: 1/1/2018
 * Time: 8:32 AM
 */

namespace App\Common\Locations\Google;


use App\MarketArea;
use GoogleMaps\GoogleMaps;
use Illuminate\Http\Request;

/**
 * Class LocationService
 * @package App\Lib\Locations\Google
 */
class LocationService {

	const ONE_KM_CHARGE = 1.7;
	const LEAST_KM_CHARGE = 6;

	/**
	 * LocationService constructor.
	 *
	 */
	public function __construct() {
//		$this->map = new GoogleMaps();
		$this->map = new ExtendedGoogleMaps();
	}

	/**
	 * @param $request Request
	 * @return
	 */
	public function googlePlaceSuggestions($request){
		$google_results = $this->map->load("placeautocomplete")
		                      ->setParam(['input'=> $request->query('address'),
			                      'components' => 'country:gh' //only search in Ghana for now
		                      ])->setEndpoint('json')->get();
		$google_results = json_decode($google_results);

		$google_results = collect($google_results->predictions)->transform(function($prediction){
			return [
				'id' => $prediction->place_id,
				'name' => $prediction->description,
				'description' => $prediction->description
			];
		});

		$collection = Collect();

		$google_results->each(function($place)use($collection){
			$collection->push($place);
		});

		return response()->json(array('total_count'=>$collection->count(),'items'=>$collection->jsonSerialize()));
	}

	public function searchGooglePlaceById( $place_id){

		$google_results = $this->map->load("placedetails")->setParam([ 'placeid'=> $place_id])->get();

		$google_results = json_decode($google_results, true);

		if(isset($google_results['error'])){
			throw new \Exception($google_results['error']);
		}else if(isset($google_results['error_message'])){
			throw new \Exception($google_results['error_message']);
		}else if(isset($google_results['status']) && $google_results['status'] == 'NOT_FOUND'){
			throw new \Exception("Google Place not found");
		}else {
			$result = $google_results['result'];

			return ([
				'latitude' => $result['geometry']['location']['lat'],
				'longitude' => $result['geometry']['location']['lng'],
				'result' => $result['formatted_address']
			]);
		}
	}

	public function deliveryCharge($coordinates, $google_place_id){

		$google_results = $this->map->load("placedetails")->setParam([ 'placeid'=> $google_place_id])->get();
		$result = json_decode($google_results, true)['result'];

		$response =  $this->map->load('distancematrix')
		                 ->setParam (['origins' => $coordinates])
		                 ->setParam (['destinations' => $result['geometry']['location']['lat'].",".$result['geometry']['location']['lng']])
		                 ->get();

		$response = json_decode($response, true);

		$meters = $response['rows'][0]['elements'][0]['distance']['value'];

		return  self::decideCharge($meters);
	}

    public function deliveryChargeByCoordinates($coordinates, $toCoordinates){


        $response =  $this->map->load('distancematrix')
            ->setParam (['origins' => $coordinates])
            ->setParam (['destinations' => $toCoordinates])
            ->get();

        $response = json_decode($response, true);

        $meters = $response['rows'][0]['elements'][0]['distance']['value'];

        return  self::decideCharge($meters);
    }


    public function addressByPlaceId($place_id){
		$google_results = $this->map->load("placedetails")
		                       ->setParam([
			                       'placeid'=> $place_id,
		                       ])->get();

		$google_results = json_decode($google_results, true);
		return $google_results['result']['formatted_address'];
	}

	public static function decideCharge($meters)
	{
		$km = $meters/1000;
		$charge = (($km) * self::ONE_KM_CHARGE);
		if($charge < self::LEAST_KM_CHARGE){
			$charge = self::LEAST_KM_CHARGE;
		}
		return round($charge);
	}
}