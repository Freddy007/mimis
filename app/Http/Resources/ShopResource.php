<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ShopResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,
            'email' => $this->email,
            'description' => $this->description,
            'banner_image' => get_cover_img_src($this, 'shop'),
            'image' => (new ImageResource($this->image)),
            'images' => $this->images,
            'rating' => $this->feedbacks->avg('rating') != null ? $this->feedbacks->avg('rating') : 0,
            'feedbacks' => FeedbackResource::collection($this->feedbacks),
            'feedback_sum' => sizeof($this->feedbacks),
            'shop_type' => $this->shop_type,
            'address'   => $this->address,
            'lat'      => $this->lat,
            'lng'      => $this->lng,
            'addresses' => $this->addresses,
            'config'    => $this->config,
            'owner'     => $this->owner,
            'external_url' => $this->external_url
        ];
    }
}
