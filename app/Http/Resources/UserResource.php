<?php

namespace App\Http\Resources;

use Exception;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{

    public static $wrap = '';

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        try{

            return [
                'user_id' => $this->id,
                'user_email' => $this->email,
                'user_name' => $this->nice_name,
                'user_phone' => $this->primaryAddress() != null ? $this->primaryAddress->phone : "",
                'user_password' => $this->password,
                'user_about_me' => $this->description,
                'added_date' =>  $this->created_at,
                'is_banned' => $this->active,
                "billing_first_name" =>  $this->name,
                "billing_last_name" => $this->name,
                "billing_company" => "",
                "billing_address_1" => $this->billingAddress != Null ? $this->billingAddress->address_line_1 : "",
                "billing_address_2" =>  $this->billingAddress != Null ? $this->billingAddress->address_line_2 : "",
                "billing_country" => "",
                "billing_state" => "",
                "billing_city" =>   $this->billingAddress != Null ? $this->billingAddress->city : "",
                "billing_postal_code" =>  "",
                "billing_email" => $this->email,
                "billing_phone" =>   $this->billingAddress != Null ? $this->billingAddress->phone : "",
                "shipping_first_name" => $this->name,
                "shipping_last_name" => $this->name,
                "shipping_company" => "",
                "shipping_address_1" =>  $this->shippingAddress != Null ? $this->shippingAddress->address_line_1 : "",
                "shipping_address_2" => $this->shippingAddress != Null ? $this->shippingAddress->address_line_2 : "",
                "shipping_country" => "",
                "shipping_state" => "",
                "shipping_city" => $this->shippingAddress != Null ? $this->shippingAddress->city : "",
                "shipping_postal_code" => "",
                "shipping_email" => $this->email,
                "shipping_phone" =>$this->shippingAddress != Null ? $this->shippingAddress->phone : "",
                "device_token" => "",
                "code" => " ",
                "verify_types" => "1",
                "country_id" => "",
                "city_id" => $this->address() != null ? $this->address()->city : "",
            ];

        }catch(Exception $e){

            return [
                'user_id' => $this->id,
                'user_email' => $this->email,
                'user_name' => $this->nice_name,
                'user_phone' =>  "",
                'user_password' => $this->password,
                'user_about_me' => $this->description,
                'added_date' =>  $this->created_at,
                'is_banned' => $this->active,
                "billing_first_name" =>  $this->name,
                "billing_last_name" => $this->name,
                "billing_company" => "",
                "billing_address_1" =>  "",
                "billing_address_2" =>  "",
                "billing_country" => "",
                "billing_state" => "",
                "billing_city" =>   "",
                "billing_postal_code" =>  "",
                "billing_email" => $this->email,
                "billing_phone" =>    "",
                "shipping_first_name" => $this->name,
                "shipping_last_name" => $this->name,
                "shipping_company" => "",
                "shipping_address_1" =>  "",
                "shipping_address_2" =>  "",
                "shipping_country" => "",
                "shipping_state" => "",
                "shipping_city" => "",
                "shipping_postal_code" => "",
                "shipping_email" => $this->email,
                "shipping_phone" => "",
                "device_token" => "",
                "code" => " ",
                "verify_types" => "1",
                "country_id" => "",
                "city_id" =>  "",
            ];
        }
      
    }


}
