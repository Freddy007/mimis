<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ShippingResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'price' => $this->rate,
            'days' => $this->delivery_takes,
            'added_date' => $this->created_at,
            'updated_date' => $this->updated_at,
            'added_user_id' => "",
            'updated_user_id' => "",
            'updated_flag'   => "0",
            'added_date_str' => "",
            'currency_symbol' => "GHS",
            'currency_short_form' => "GHS"
        ];
    }



}
