<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [

            'id'  => $this->id,
            'shop_id' => $this->shop_id,
            'manufacturer_id' => $this->manufacturer_id,
            'brand'  => $this->brand,
            'name' => $this->name,
            'model_number' => $this->model_number ,
            'mpn' => $this->mpn,
            'gtin' => $this->gtin,
            'gtin_type' => $this->gtin_type,
            'description' => $this->description,
            'min_price' => $this->min_price,
            'max_price' => $this->max_price,
            'sale_price' => $this->sale_price,
            'on_sale' => $this->on_sale,
            'price'     => $this->price,
            'rating' => $this->feedbacks->avg('rating') != null ? $this->feedbacks->avg('rating') : 0,
            'feedbacks_sum' => sizeof($this->feedbacks),
            'origin_country' => $this->origin_country,
            'has_variant' => $this->has_variant,
            'requires_shipping' => $this->requires_shipping,
            'downloadable' => $this->downloadable,
            'slug' => $this->slug,
            // 'meta_title',
            // 'meta_description',
            'sale_count' => $this->sale_count,
            'active' =>  $this->active,
            'service' => $this->service,
            'images'   => $this->images,
            'shop'    => $this->shop,
            'categories' => $this->categories,
            'created_at' => $this->created_at,
            'feedbacks' => FeedbackResource::collection($this->feedbacks),
            'wishlist'   => $this->wishlist,
	        'wishlists' => $this->wishlists
        ];
    }
}
