<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\ListHelper;
use Illuminate\Http\Request;
use App\Common\Authorizable;
use Yajra\Datatables\Datatables;
use App\Http\Controllers\Controller;
use App\Repositories\Product\ProductRepository;
use App\Http\Requests\Validations\CreateProductRequest;
use App\Http\Requests\Validations\UpdateProductRequest;

class ProductController extends Controller
{
    use Authorizable;

    private $model;

    private $product;

    /**
     * construct
     */
    public function __construct(ProductRepository $product)
    {
        parent::__construct();
        $this->model = trans('app.model.product');
        $this->product = $product;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $products = $this->product->all();

        $trashes = $this->product->trashOnly();

        return view('admin.product.index', compact('trashes'));
    }

    // function will process the ajax request
    public function getProducts(Request $request) {

        $products = $this->product->all();

        return Datatables::of($products)
            ->addColumn('option', function ($product) {
                return view( 'admin.partials.actions.product.options', compact('product'));
            })
            ->editColumn('name', function($product){
                return view( 'admin.partials.actions.product.name', compact('product'));
            })
            ->editColumn('gtin', function($product){
                return view( 'admin.partials.actions.product.gtin', compact('product'));
            })
            ->editColumn('category',  function ($product) {
                return view( 'admin.partials.actions.product.category', compact('product'));
            })
            ->editColumn('inventories_count', function($product){
                return view( 'admin.partials.actions.product.inventories_count', compact('product'));
            })
            ->rawColumns([ 'name', 'gtin', 'category', 'inventories_count', 'status', 'option' ])
            ->make(true);
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.product.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateProductRequest $request)
    {
        $this->authorize('create', \App\Product::class); // Check permission

        $product = $this->product->store($request);

        $request->session()->flash('success', trans('messages.created', ['model' => $this->model]));

        if ($request->has("service_page")){
            return response()->json($this->getJsonParams($product, true));
        }

        return response()->json($this->getJsonParams($product));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {
        $product = $this->product->find($id);

        $this->authorize('view', $product); // Check permission

        return view('admin.product._show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = $this->product->find($id);

        $this->authorize('update', $product); // Check permission

        $preview = $product->previewImages();

        return view('admin.product.edit', compact('product', 'preview'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProductRequest $request, $id)
    {
        $product = $this->product->update($request, $id);

        $this->authorize('update', $product); // Check permission

        $request->session()->flash('success', trans('messages.updated', ['model' => $this->model]));

        if ($request->has('service_page')){
            return response()->json($this->getJsonParams($product, true));
        }

        return response()->json($this->getJsonParams($product));
    }

    /**
     * Trash the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function trash(Request $request, $id)
    {
        $this->product->trash($id);

        return back()->with('success', trans('messages.trashed', ['model' => $this->model]));
    }

    /**
     * Restore the specified resource from soft delete.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore(Request $request, $id)
    {
        $this->product->restore($id);

        return back()->with('success', trans('messages.restored', ['model' => $this->model]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $this->product->destroy($id);

        return back()->with('success',  trans('messages.deleted', ['model' => $this->model]));
    }

    /**
     * return json params to proceed the form
     *
     * @param Product $product
     *
     * @param boolean $is_service
     * @return array
     */
    private function getJsonParams($product, $is_service=false){
        return [
            'id' => $product->id,
            'model' => 'product',
            'redirect' => $is_service ? route('admin.catalog.product.services') : route('admin.catalog.product.index')
        ];
    }

    public function services()
    {
        $trashes = $this->product->trashOnly();

        return view('admin.service.index', compact('trashes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function serviceCreate()
    {
        // return ListHelper::serviceCategories();

        $gtin_types = ListHelper::gtin_types();
        $tags        = ListHelper::tags();
        $categories  = ListHelper::serviceCategories();
        $countries  = ListHelper::countries();
        $manufacturers  = ListHelper::manufacturers();
        return view('admin.service.create', compact("gtin_types", "tags", "categories", "countries", "manufacturers"));
    }

    // function will process the services ajax request
    public function getServices(Request $request) {

        $services = $this->product->allServices();

        return Datatables::of($services)
            ->addColumn('option', function ($product) {
                return view( 'admin.partials.actions.service.options', compact('product'));
            })
            ->editColumn('name', function($product){
                return view( 'admin.partials.actions.product.name', compact('product'));
            })
            ->editColumn('gtin', function($product){
                return view( 'admin.partials.actions.product.gtin', compact('product'));
            })
            ->editColumn('category',  function ($product) {
                return view( 'admin.partials.actions.product.category', compact('product'));
            })
            ->editColumn('inventories_count', function($product){
                return view( 'admin.partials.actions.product.inventories_count', compact('product'));
            })
            ->rawColumns([ 'name', 'gtin', 'category', 'inventories_count', 'status', 'option' ])
            ->make(true);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function serviceEdit($id)
    {
        $product = $this->product->find($id);

        $this->authorize('update', $product); // Check permission

        $preview = $product->previewImages();

        $tags              = ListHelper::tags();
        $categories        = ListHelper::categories();

        return view('admin.service.edit', compact('product', 'preview', 'tags','categories'));
    }

}