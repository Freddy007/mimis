<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\UserResource;
use App\User;
use App\Customer;
use App\UserInterest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Validations\CreateCustomerRequest;
use App\Notifications\Auth\SendVerificationEmail as EmailVerificationNotification;
use Exception;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
//     $successStatus = 200;

    /**
     * Handles Registration Request
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        try{

        // $this->validate($request, [
        //     'name' => 'required|min:3',
        //     'email' => 'required|email|unique:customers',
        //     'password' => 'required|min:6',
        // ]);

        if(Customer::whereEmail($request->email)->first()){
            return response()->json(['status' => 'error','message' => 'Sorry, Email already exists!'], 500); 
        }

        $customer = Customer::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'accepts_marketing' => $request->has("subscribe")?$request->subscribe : false,
            'verification_token' => str_random(40),
            'active' => 1
        ]);

        // Sent email address verification notif to customer
        $customer->notify(new EmailVerificationNotification($customer));

        $customer->addresses()->create($request->all()); //Save address

        $token = $customer->createToken('TutsForWeb')->accessToken;

        $success['token'] = $token;
        $success['userId'] = $customer->id;
        $success['name'] = $customer->name;
        $success['email'] = $customer->email;

        $customer = Customer::find($success['userId']);
        return new UserResource($customer);


        }catch(Exception $e){
            // return response()->json([ "error" => $e->getMessage(), "trace" => $e->getTraceAsString()], 500);

            return response()->json(['status' => 'error','message' => 'Sorry, Problem creating account!', 'error' => $e->getMessage()], 500);
        }

    }

    /**
     * Handles Login Request
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $credentials = [
            'email' => $request->email,
            'password' => $request->password
        ];

        if (auth()->guard('customer')->attempt($credentials)) {
            $success['token'] =  auth()->guard('customer')->user()->createToken('TutsrWeb')->accessToken;
            $success['userId'] = auth()->guard('customer')->user()->id;
            $success['name'] = auth()->guard('customer')->user()->name;
            $success['email'] = auth()->guard('customer')->user()->email;

            $customer = Customer::find($success['userId']);
            // $customer = Customer::whereId($success['userId']);
            // $loader = $customer->with("primaryAddress")->get();

            // return $loader;
            return new UserResource($customer);

//            return response()->json([ $success], 200, ["x-auth-token" => [$success['token']]]);

        } else {
            return response()->json(['status' => 'error','message' => 'Sorry, User Login Credentials Wrong'], 404);
        }

    }

    public function update(Request $request)
    {
        $user_id = $request->user_id;
        $first_name = $request->billing_first_name;
        $last_name = $request->billing_last_name;
        $address_line_1 = $request->billing_address_1;
        $address_line_2 = $request->billing_address_2;
        $city = $request->billing_city;
        $phone = $request->user_phone;
        $email  = $request->user_email;

        $builder = Customer::find($user_id);
        $customer = $builder->update([
            "name" => $first_name 
        ]);

        $address = $builder->primaryAddress()
        ->updateOrCreate ([
            "address_line_1" => $address_line_1,
            "address_line_2" => $address_line_2,
            "city"          => $city,
            "phone"         => $phone
        ]);

        // $builder->billingAddress()->updateOrCreate ([
        //     "address_line_1" => $address_line_1,
        //     "address_line_2" => $address_line_2,
        //     "city"          => $city,
        //     "phone"         => $phone
        // ]);

        // $builder->shippingAddress()->updateOrCreate ([
        //     "address_line_1" => $address_line_1,
        //     "address_line_2" => $address_line_2,
        //     "city"          => $city,
        //     "phone"         => $phone
        // ]);


        return new UserResource($builder);

    }

    /**
     * Returns Authenticated User Details
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function details()
    {
        return response()->json(['user' => auth()->user()], 200);
    }

    protected function validator(array $data)
{
    return Validator::make($data, [
        'name' => ['required', 'string', 'max:255'],
        'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
        'password' => ['required', 'string', 'min:6', 'confirmed'],
    ]);
}
}