<?php

namespace App\Http\Controllers\Api;

use App\Common\PushyAPI;
use App\Jobs\ProcessPaidOrders;
use App\Jobs\ProcessUnPaidOrders;
use App\OrderBatch;
use App\PaymentMethod;
use DB;
use Auth;
use App\Shop;
use App\Cart;
use App\Order;
use Illuminate\Http\Request;
use App\Events\Order\OrderCreated;
use App\Http\Controllers\Controller;
use App\Http\Resources\OrderResource;
use App\Http\Requests\Validations\DirectCheckoutRequest;
use Illuminate\Support\Collection;

class CheckoutController extends Controller
{
    /**
     * Checkout the specified cart.
     *
     * @param \Illuminate\Http\Request $request
     * @param $cartIds
     * @param $deliveryCost
     * @param $totalWithDeliveryCharge
     * @param $pushyToken
     * @param $loginUserId
     * @return \Illuminate\Http\Response
     */

    public function flutterPaymentsRedirect(Request $request, $cartIds,$deliveryCost,$totalWithDeliveryCharge,$pushyToken,$loginUserId){
        try {

            if ( $request->query("status") !== Order::SUCCESSFUL) {
                $data = array('type' => 'payment',
                    'message' => ['title' => "payments failed", 'content' => "payments failed!"],
                    'has_image' => false,
                    'image' => '',
                    'has_link' => false,
                    'link' => ''
                );

                PushyAPI::sendPushNotification($data, [$pushyToken], []);
                return response()->json("Failed to checkout! ", 500);
            }


            if ( $request->query("status") === Order::SUCCESSFUL)
                $transaction_id = $request->query("tx_ref");
                $mode_of_payment = "momo";
                $payment_gateway_status = $request->query("status");

                $paymentStatus = Order::PAYMENT_STATUS_PAID;

                $request->merge(['transaction_id' => $transaction_id]);
                $request->merge(['mode_of_payment' => $mode_of_payment]);
                $request->merge(['payment_gateway_status' => $payment_gateway_status]);
                $request->merge(['cart_ids' => $cartIds]);
                $request->merge(['total_with_delivery_charge' => $totalWithDeliveryCharge]);
                $request->merge(['delivery_charge' => $deliveryCost]);
                $request->merge(['customer_id' => $loginUserId]);

//                $cartIdsArray = explode(",", $request->get("cart_ids"));
                 $cartIdsArray = explode(",", $cartIds);

                $this->saveOrderBatch($request);

                ProcessPaidOrders::dispatch($request, $cartIdsArray, $paymentStatus, $pushyToken);

                return \response()->json(["message" => "success"], 200);

//            return $orders;

        }catch (\Exception $e){

            return response()->json("Failed to checkout! " . $e->getMessage(), 500);

        }
    }


    public function saveOrderBatch(Request $request){
        $cart_ids = explode(",", $request->get("cart_ids"));

        $carts = Cart::whereIn("id",$cart_ids);

        $carts = $carts->get();

        $carts->load(['inventories.image']);

        $itemCollection = new Collection();

        foreach ($carts as $cart){
            foreach ($cart->inventories as $inventory){
                $itemCollection->push($inventory->pivot);
            }
        }

        OrderBatch::create([
            "order_number" => get_formated_order_number($request->customer_id),
            "customer_id" => $request->customer_id,
            "delivery_charge" => $request->delivery_charge,
            "total_with_delivery_charge" => $request->total_with_delivery_charge,
            "order_items" => $itemCollection
        ]);

        return $itemCollection;
    }

    public function checkout(Request $request, Cart $cart)
    {
        if( !crosscheckCartOwnership($request, $cart) )
            return response()->json(['message' => trans('theme.notify.please_login_to_checkout')], 404);

        // Update the cart
        $cart->shipping_rate_id = $request->shipping_option_id;
        $cart->packaging_id = $request->packaging_id;
        $cart->payment_method_id = $request->payment_method_id;

        if($request->shipping_option_id)
            $cart->shipping = getShippingingCost($request->shipping_option_id);

        if($request->packaging_id)
            $cart->packaging = getPackagingCost($request->packaging_id);

        $cart->save();

        $cart = crosscheckAndUpdateOldCartInfo($request, $cart);

        // Start transaction!
        DB::beginTransaction();
        try {
            // Create the order from the cart
            $order = saveOrderFromCart($request, $cart);

        } catch(\Exception $e){
            \Log::error($e);        // Log the error

            // rollback the transaction and log the error
            DB::rollback();

            return response()->json(trans('theme.notify.order_creation_failed'), 500);
        }

        // Everything is fine. Now commit the transaction
        DB::commit();

        $cart->forceDelete();   // Delete the cart

//        event(new OrderCreated($order));   // Trigger the Event

        return new OrderResource($order);
    }

    public function mobileCheckout(Request $request)
    {

        try {

            $cart_ids = explode(",", $request->get("cart_ids"));

            $this->saveOrderBatch($request);

            ProcessUnPaidOrders::dispatch($request, $cart_ids);

            return \response()->json("Success!", 200);

        }catch (\Exception $e){
            return \response()->json("Failed to checkout!", 500);
        }
    }


    private function startOrderProcess(Request $request, $cart_id,$payment_status=NULL){
        $cart = Cart::whereId($cart_id)->first();

        if( !crosscheckCartOwnership($request, $cart) )
            return response()->json(['message' => trans('theme.notify.please_login_to_checkout')], 404);

        // Update the cart
        $cart->shipping_rate_id = $request->shipping_option_id;
        $cart->packaging_id = $request->packaging_id;
        $cart->payment_method_id = $payment_status != NULL ? PaymentMethod::TYPE_FLUTTER : $request->payment_method_id;
        $cart->customer_id       = $request->customer_id;

//        if($request->shipping_option_id)
//            $cart->shipping = getShippingingCost($request->shipping_option_id);

//        if($request->packaging_id)
//            $cart->packaging = getPackagingCost($request->packaging_id);

        $cart->save();

        $cart = crosscheckAndUpdateOldCartInfo($request, $cart);

        // Start transaction!
        DB::beginTransaction();
        try {
            // Create the order from the cart
            $paymentStatus = $payment_status!=NULL ? $payment_status : Order::PAYMENT_STATUS_UNPAID;
            $order = saveOrderFromCart($request, $cart, $paymentStatus);

        } catch(\Exception $e){
            \Log::error($e);        // Log the error

            // rollback the transaction and log the error
            DB::rollback();

            return response()->json(["error" => $e->getMessage(),trans('theme.notify.order_creation_failed')], 500);
        }

        // Everything is fine. Now commit the transaction
        DB::commit();

        $cart->forceDelete();   // Delete the cart

        event(new OrderCreated($order));   // Trigger the Event

//        $ new OrderResource($order);
        return new OrderResource($order);
    }


    /**
     * Direct checkout with the item/cart
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  str $slug
     *
     * @return \Illuminate\Http\Response
     */
    // public function directCheckout(DirectCheckoutRequest $request, $slug)
    // {
    //     $cart = $this->addToCart($request, $slug);

    //     if (200 == $cart->status())
    //         return redirect()->route('cart.index', $cart->getdata()->id);
    //     else if (444 == $cart->status())
    //         return redirect()->route('cart.index', $cart->getdata()->cart_id);

    //     return redirect()->back()->with('warning', trans('theme.notify.failed'));
    // }

}


