<?php

namespace App\Http\Controllers\Api;

use App\CategoryGroup;
use App\Customer;
use App\Http\Resources\ImageResource;
use App\Http\Resources\ProductResource;
use App\Http\Resources\ShopResource;
use App\Image;
use App\Order;
use App\OrderBatch;
use App\OrderItem;
use App\Service;
use App\Shop;
use App\Product;
use App\Category;
use App\Inventory;
use App\Manufacturer;
use Carbon\Carbon;
use App\Helpers\ListHelper;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\ItemResource;
use App\Http\Resources\ListingResource;
use App\Wishlist;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ListingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $list = 'latest', $limit = 15)
    {

            switch ($list) {
                case 'trending':
                    $listings = ListHelper::popular_items(config('system.popular.period.trending', 2), config('system.popular.take.trending', $limit));
                    break;

                case 'popular':
                    $listings = ListHelper::popular_items(config('system.popular.period.weekly', 7), config('system.popular.take.weekly', 5));
                    break;

                case 'random':
                    $listings = ListHelper::random_items(10);
                    break;

                case 'services':
                    $listings = ListHelper::random_services(10);
                    break;

                case 'latest':
                default:
                    $listings = ListHelper::latest_available_items(10);
                    break;
            }

            // return $listings;

            return ListingResource::collection($listings);
    }

    public function offers(Request $request)
    {
        $products = Inventory::where('active', 1)->get();

        $products->load(['shop']);
        // Keep results only from active shops

        $products = $products->filter(function ($product) {
            if ($product->shop !== Null) {
                return ($product->shop->current_billing_plan !== Null) && ($product->shop->active == 1);
            }
        });

        $products = $products->where('stock_quantity', '>', 0)->where('available_from', '<=', Carbon::now());

        if($request->has('free_shipping')) {
            $products = $products->where('free_shipping', 1);
        }
        if($request->has('new_arrivals')) {
            $products = $products->where('created_at', '>', Carbon::now()->subDays(config('system.filter.new_arrival', 14)));
//            $products = $products->where('created_at', '>', Carbon::now()->subDays( 100));
        }

        if($request->has('has_offers')) {
            $products = $products->where('offer_price', '>', 0);
//                ->where('offer_start', '<', Carbon::now())
//                ->where('offer_end', '>', Carbon::now());
        }

        if($request->has('condition')) {
            $products = $products->whereIn('condition', array_keys(request()->input('condition')));
        }

        if($request->has('price')) {
            $price = explode('-', request()->input('price'));
            $products = $products->where('sale_price', '>=', $price[0])->where('sale_price', '<=', $price[1]);
        }


        $products = $products->paginate(config('system.view_listing_per_page', 16));

        // return json_encode($products);
        return ListingResource::collection($products);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search()
    {
	    $inventoryProductIDs = Inventory::groupBy("product_id")->pluck('id')->toArray();

//        $products = Inventory::search($term)->where('active', 1)->get();
        if (request()->has("term") && request()->input("term") == "all") {
            $builder = Inventory::whereIn("id", $inventoryProductIDs)->where('active', 1);

            if (request()->has('low_to_high')){
                $builder = $builder->orderBy("sale_price","asc")->get();
            }elseif (request()->has('high_to_low')){
                $builder = $builder->orderBy("sale_price","desc")->get();
            }else{
                $builder = $builder->get();
            }

        }elseif (request()->has("term")) {
            $builder = Inventory::whereIn("id", $inventoryProductIDs)
                                ->where("title", "LIKE", "%". request()->input("term"). "%")
                ->where('active', 1);

            if (request()->has('low_to_high')){
                $builder = $builder->orderBy("sale_price","asc")->get();
            }elseif (request()->has('high_to_low')){
                $builder = $builder->orderBy("sale_price","desc")->get();
            }else{
                $builder = $builder->get();
            }
        }

        $products = $builder->load(['shop']);
        // Keep results only from active shops
        $products = $products->filter(function ($product) {
            return ($product->shop != Null && $product->shop->current_billing_plan !== Null) && ($product->shop->active == 1);
        });

        $products = $products->where('stock_quantity', '>', 0)
            ->where('available_from', '<=', Carbon::now());

        if(request()->has('free_shipping')) {
            $products = $products->where('free_shipping', 1);
        }
        if(request()->has('new_arrivals')) {
//            return request()->input("new_arrivals");
            $products = $products->where('created_at', '>',
                Carbon::now()->subDays(config('system.filter.new_arrival', 7)));
        }

        if(request()->has('has_offers')) {
            $products = $products->where('offer_price', '>', 0)
            ->where('offer_start', '<', Carbon::now())
            ->where('offer_end', '>', Carbon::now());
        }

        if(request()->has('condition')) {
            $condition = explode(",",request()->input('condition'));
//            $products = $products->whereIn('condition', array_keys(request()->input('condition')));
            $products = $products->whereIn('condition', $condition);
        }

//        if(request()->has('condition')) {
//            $products = $products->whereCondition( request()->input('condition'));
//        }

        if(request()->has('price')) {
            $price = explode('-', request()->input('price'));
            $products = $products->where('sale_price', '>=', $price[0])->where('sale_price', '<=', $price[1]);
        }


        $products = $products->paginate(config('system.view_listing_per_page', 16));

        // return json_encode($products);
        return ListingResource::collection($products);
    }

    /**
     * Display a listing of the resource.
     *
     * @param  str $slug item_slug
     *
     * @return \Illuminate\Http\Response
     */
    public function item($slug)
    {
        $item = Inventory::where('slug', $slug)->available()->withCount('feedbacks')->firstOrFail();

        $item->load(['product' => function($q){
                $q->select('id', 'slug', 'manufacturer_id')
                ->withCount(['inventories' => function($query){
                    $query->available();
                }]);
            }, 'attributeValues' => function($q){
                $q->select('id', 'attribute_values.attribute_id', 'value', 'color', 'order')
                ->with('attribute:id,name,attribute_type_id,order');
            },
            'feedbacks.customer:id,nice_name,name',
            'images:path,imageable_id,imageable_type',
        ]);

        // $variants = ListHelper::variants_of_product($item, $item->shop_id);

        // $attr_pivots = \DB::table('attribute_inventory')->select('attribute_id','inventory_id','attribute_value_id')
        // ->whereIn('inventory_id', $variants->pluck('id'))->get();

        // $item_attrs = $attr_pivots->where('inventory_id', $item->id)->pluck('attribute_value_id')->toArray();

        // $attributes = \App\Attribute::select('id','name','attribute_type_id','order')
        // ->whereIn('id', $attr_pivots->pluck('attribute_id'))
        // ->with(['attributeValues' => function($query) use ($attr_pivots) {
        //     $query->whereIn('id', $attr_pivots->pluck('attribute_value_id'))->orderBy('order');
        // }])->orderBy('order')->get();

        // $variants = $variants->toJson(JSON_HEX_QUOT);

        // // TEST
        // $related = ListHelper::related_products($item);
        // $linked_items = ListHelper::linked_items($item);

        // $geoip = geoip(request()->ip()); // Set the location of the user

        return new ItemResource($item);
    }

    /**
     * Display a listing of the resource.
     *
     * @param  str $slug category_slug
     *
     * @return \Illuminate\Http\Response
     */
    public function category($slug)
    {
        $category = Category::where('slug', $slug)->active()->firstOrFail();

        // Take only available items
        $all_products = $category->listings()->available();

        // Filter results
        $listings = $all_products->filter(request()->all())
        ->withCount(['feedbacks', 'orders' => function($q){
            $q->withArchived();
        }])
        ->with(['feedbacks:rating,feedbackable_id,feedbackable_type', 'image:path,imageable_id,imageable_type'])
        ->paginate(config('system.view_listing_per_page', 16))->appends(request()->except('page'));

        return ListingResource::collection($listings);
    }

    public function shops(Request $request ){

//    	$inventoryShops = Inventory::where("offer","<>",null)->groupBy("shop_id")
//	                                                         ->pluck("shop_id")->toArray();

        $builder = Shop::with(["owner" => function ($o) {
            return $o->with("image")->get();
        }, "addresses", "feedbacks" => function ($f) {
            return $f->with(["customer"])->get();
        }, "image", "images", "config"]);

        $limit = $request->has("limit") ? $request->query("limit") : 15;
        if ($request->has("address")) {
                $shops = $builder->where("address", "LIKE", "%" . $request->address . "%")
                ->active()
                ->limit($limit)
                ->get();

        }elseif ($request->has("term")){
            $term_builder = $builder->where("name","LIKE","%".$request->query('term')."%");

            $shops = $term_builder
                ->active()
                ->limit($limit)
                ->get();
        }else{
            $shops= $builder->inRandomOrder()
                ->active()
                ->limit($limit)
                ->get();
        }

        return ShopResource::collection($shops);
    }

    /**
     * Display a listing of the shop.
     *
     * @param  str $slug shop_slug
     *
     * @return [type]       [description]
     */
    public function shop(Request $request, $slug)
    {
        $shop = Shop::where('slug', $slug)->active()->firstOrFail();

        // Check shop maintenance_mode
        if($shop->isDown())
            return response()->json(['message' => trans('app.marketplace_down')], 404);

        $listings = Inventory::where('shop_id', $shop->id)
            ->filter(request()->all())
        ->with(['product' => function($p){
            return $p->with("categories")->get();
        },'shop', 'feedbacks' => function($f){
            return $f->with(["customer"])->get();
        }, 'images'])
        ->withCount(['orders' => function($q){
            $q->withArchived();
        }])->active()
        ->available()
        ->paginate(20);

        return ($listings);
    }

    public function allShopsListings()
    {
        $shopListings = Shop::with(["owner" => function($o){
            return $o->with("image")->get();
        },"addresses","config","images",
            "inventories" => function($i){
            return $i->with(["shop" =>function($s){
                return $s->with(["addresses", "owner" => function($o){
                    return $o->with("image")->get();
                }])->get();
            },'feedbacks'=> function($f){
                return $f->with(["customer"])->get();
            },'images'])
                ->inRandomOrder()
                ->active()
                ->limit(5)->get();
        }])->inRandomOrder()
        ->active()
        ->paginate();

        return response()->json($shopListings,200);
    }

    

    public function  allSliderImages(){
        $images = Image::whereImageableType("App\Inventory")
            ->inRandomOrder()
            ->limit(5)->get();

        return $images;
    }

    public function allListings(){

      if( request()->has("shop_id") && request()->has("category_id")) {

            $cat_array = [request()->query("category_id")];

            $productIDs = \DB::table('category_product')
                ->whereIn('category_id', $cat_array)->pluck('product_id')->toArray();

             $filteredInventoryBuilder = Inventory::whereIn("product_id", $productIDs);
             $filteredInventoryIDs = $this->shopIdExist() ?
                 $filteredInventoryBuilder  ->groupBy("product_id")->distinct()->pluck("id")->toArray():
                 $filteredInventoryBuilder->whereShopId(request()->query("shop_id"))
                    ->groupBy("product_id")->distinct()->pluck("id")->toArray();

          $builderHolder = Inventory::whereIn('id', $filteredInventoryIDs);

                $builder = $this->shopIdExist() ?
                    $builderHolder->with(["feedbacks" => function($feedback){
                    return $feedback->with(["customer"])->get();}])
                    ->available() :
                    $builderHolder->whereShopId(request()
                        ->query("shop_id")) ->with(["feedbacks" => function($feedback){
                    return $feedback->with(["customer"])->get();}])
                    ->available();


            if (request()->has('filter') && (request()->input('filter') == "low_to_high")){
                $builder = $builder->orderBy("sale_price","asc")->distinct()->get();
            }elseif (request()->has('filter') && (request()->input('filter') == "high_to_low")){
                $builder = $builder->orderBy("sale_price","desc")->distinct()->get();
            }else{
                $builder = $builder->inRandomOrder()->distinct()->get();
            }

            }elseif (request()->has("shop_id")){

                $builder = Inventory::whereShopId(request()->query("shop_id"))
                ->with(["feedbacks" => function($feedback){ 
                    return $feedback->with(["customer"])->distinct()->get();}])->available();
                if (request()->has('filter') && (request()->input('filter') == "low_to_high")){
                    $builder = $builder->orderBy("sale_price","asc")->distinct()->get();
                }elseif (request()->has('filter') && (request()->input('filter') == "high_to_low")){
                    $builder = $builder->orderBy("sale_price","desc")->distinct()->get();
                }else{
                    $builder = $builder->inRandomOrder()->distinct()->get();
                }
            

        }else{
	      $filteredInventoryIDs = Inventory::groupBy("product_id")->distinct()->pluck("id")->toArray();

            $builder = Inventory::whereIn("id", $filteredInventoryIDs)
                ->with(["feedbacks" => function($feedback){ return $feedback->with(["customer"])->get();}])->available();
            if (request()->has('filter') && (request()->input('filter') == "low_to_high")){
                $builder = $builder->orderBy("sale_price","asc")->distinct()->get();
            }elseif (request()->has('filter') && (request()->input('filter') == "high_to_low")){
                $builder = $builder->orderBy("sale_price","desc")->distinct()->get();
            }else{
                $builder = $builder->inRandomOrder()->distinct()->get();
            }
        }
       
        $products = $builder->load(['wishlists','wishlist','shop'=> function($shop){
            return $shop->with(["addresses","owner" => function($owner){return $owner->with("image")->get();}])->get();}]);
        // Keep results only from active shops
        $products = $products->filter(function ($product) {
            return ($product->shop != Null && $product->shop->current_billing_plan !== Null) &&
                ($product->shop->active == 1) &&
                ($product->shop->whereNull('trial_ends_at')
                    ->orWhere('trial_ends_at', '>', Carbon::now()));
        });

        $products = $products->where('stock_quantity', '>', 0)
            ->where('available_from', '<=', Carbon::now());
//            ->where('available_from', '<=', Carbon::yesterday());

        if(request()->has('filter') && (request()->input('filter') == "free_shipping") ) {
            $products = $products->where('free_shipping', 1);
        }

        if(request()->has('filter') && (request()->input('filter') == "new_arrivals")) {

            $products = $products->where('created_at', '>',
                Carbon::now()->subDays(30));
        }

        if(request()->has('filter') && (request()->input('filter') == "has_offers")) {
            $products = $products->where('offer_price', '>', 0)
//                ->where('offer_start', '<', Carbon::now())
//                ->where('offer_end', '>', Carbon::now())
            ;
        }

        if( (request()->has("filter") && request()->has('condition'))) {
            $condition = explode(",",request()->input('condition'));
//            $products = $products->whereIn('condition', array_keys(request()->input('condition')));
            $products = $products->whereIn('condition', $condition);
        }

        if(request()->has('price')) {
            $price = explode('-', request()->input('price'));
            $products = $products->where('sale_price', '>=', $price[0])->where('sale_price', '<=', $price[1]);
        }

        $products = $products->paginate(10);

        return ListingResource::collection($products);
    }


    public function singleShopListing($shop_id){

    	$inventoryProductIDs = Inventory::whereShopId($shop_id)->groupBy("product_id")->pluck('id')->toArray();

        $builder = Inventory::whereShopId($shop_id)->whereIn("id", $inventoryProductIDs)
        ->with(["feedbacks" => function($feedback){ return $feedback->with(["customer"])->get();}])->available();
        if (request()->has('filter') && (request()->input('filter') == "low_to_high")){
            $builder = $builder->orderBy("sale_price","asc")->distinct()->get();
        }elseif (request()->has('filter') && (request()->input('filter') == "high_to_low")){
            $builder = $builder->orderBy("sale_price","desc")->distinct()->get();
        }else{
            $builder = $builder->inRandomOrder()->distinct()->get();
        }

        $products = $builder->load(['wishlist','shop'=> function($shop){
            return $shop->with(["owner" => function($owner){return $owner->with("image")->get();}])->get();}]);
        // Keep results only from active shops
        $products = $products->filter(function ($product) {
            return ($product->shop != Null && $product->shop->current_billing_plan !== Null) &&
                ($product->shop->active == 1) &&
                ($product->shop->whereNull('trial_ends_at')->orWhere('trial_ends_at', '>', Carbon::now()));
        });

        $products = $products->where('stock_quantity', '>', 0)
            ->where('available_from', '<=', Carbon::now());

        if(request()->has('filter') && (request()->input('filter') == "free_shipping") ) {
            $products = $products->where('free_shipping', 1);
        }

        if(request()->has('filter') && (request()->input('filter') == "new_arrivals")) {
            $products = $products->where('created_at', '>',
                Carbon::now()->subDays(config('system.filter.new_arrival', 7)));
        }

        if(request()->has('filter') && (request()->input('filter') == "has_offers")) {
            $products = $products->where('offer_price', '>', 0)
                ->where('offer_start', '<', Carbon::now())
                ->where('offer_end', '>', Carbon::now());
        }

        if( (request()->has("filter") && request()->has('condition'))) {
            $condition = explode(",",request()->input('condition'));
//            $products = $products->whereIn('condition', array_keys(request()->input('condition')));
            $products = $products->whereIn('condition', $condition);
        }


        if(request()->has('price')) {
            $price = explode('-', request()->input('price'));
            $products = $products->where('sale_price', '>=', $price[0])->where('sale_price', '<=', $price[1]);
        }

        $products = $products->paginate(config('system.view_listing_per_page', 16));

        // return json_encode($products);
        return ListingResource::collection($products);
    }


    /**
     * Open brand page
     *
     * @param  slug  $slug
     * @return \Illuminate\Http\Response
     */
    public function brand($slug)
    {
        $brand = Manufacturer::where('slug', $slug)->firstOrFail();

        $ids = Product::where('manufacturer_id', $brand->id)->pluck('id');

        $listings = Inventory::whereIn('product_id', $ids)->filter(request()->all())
        ->whereHas('shop', function($q) {
            $q->select(['id', 'current_billing_plan', 'active'])->active();
        })
        ->with(['feedbacks:rating,feedbackable_id,feedbackable_type', 'image:path,imageable_id,imageable_type'])
        ->withCount(['orders' => function($q){
            $q->withArchived();
        }])
        ->active()->paginate(20);

        return ListingResource::collection($listings);
    }

    public function serviceListings(Request $request, $list=10){

        if ($request->has("term")) {
            $listings = ListHelper::search_services($request->query("term"));
            return ProductResource::collection($listings);
        }elseif ($request->has("shop")){

            $listings = ListHelper::shop_services($request->query("shop"));
            return ProductResource::collection($listings);

        }elseif ($request->has("offers")){
            $listings = ListHelper::sale_services($list);
            return ProductResource::collection($listings);

        }else{
            $listings = ListHelper::random_services($list);
            // return $listings;
            return ProductResource::collection($listings);
        }
    }

    public function getImageListings($id){
        $listings = ListHelper::images($id);
////        return $listings;
        return ImageResource::collection($listings);
    }

    public function listingImages($id){

        $inventory = Inventory::with(["images"])
            ->whereId($id)->first();
        return $inventory;
    }

    public function serviceImages($id){
        $inventory = Service::with(["images"])
            ->whereId($id)->first();
        return $inventory;
    }

    public function getShopImageListings($id){

        $shop = Shop::with(["images"])
            ->whereId($id)->get();
        return ShopResource::collection($shop);
    }

    public function categoryProducts(Request $request, $shop_id){
        $limit = $request->has("limit") ? $request->query('limit') : 5;
        $date = Carbon::now();

        // return Inventory::with("categories")->get();
        // $category_ids = Category::inRandomOrder()->pluck('id');
        // $productIDs = \DB::table('category_product')->whereIn('category_id', $category_ids)->pluck('product_id')->toArray();

        $categories =  Category::with(["listings" ,"products" => function( $q) use ($shop_id, $date) {
               $q->with(["images","inventories" => function($i){
               return $i->with(["images","shop" => function($s){
                   $s->with(["owner" => function($o){
                       $o->with("image")->get();
                   }]);
               }, "feedbacks" => function($f){
                return $f->with(["customer"])->get();
               }])->inRandomOrder()->paginate();
               // product and ib
           }])->inRandomOrder()
               ->whereService(false)
              ->whereShopId($shop_id)
           ->get();
       }])->whereHas("products")->paginate();
        return $categories;
    }




    public function allCategoryProducts(){
        $categories =  Category::with(["subGroup" => function($s){
            return $s->with(["group"])->get();
        },"products" => function( $q) {
            return $q->with(["images","shop"=> function($s){
                return $s->with(["addresses","images","image","config"]);}])->get();
        }])->get();
        return $categories;
    }

    public function shopsByCategory($slug){

        $shops = Shop::with(["addresses","images","image","config"])
            ->leftJoin("products","shops.id","=","products.shop_id")
            ->leftJoin("category_product", "products.id","=","category_product.product_id")
            ->leftJoin("categories","categories.id","=","category_product.category_id")
            ->whereRaw("categories.slug = '$slug'")
            ->selectRaw("shops.*, categories.name as category_name")
            ->distinct()
            ->get();

        return $shops;
    }

    public function categoryProductsById($category_id){

        return CategoryGroup::with("categories")->get();
    }

    public function shopCategories(){
        $categories = Category::with(["products" => function($p){
            return $p->with("inventories")->get();
        }])->whereActive(true)->get();

        return $categories;
    }


    public function categoryShops($category_id){
        $categories =  Category::with(["products" => function( $q)  {
            return $q->with("shop")->first();
        }])->whereId($category_id)->get();
//        return $listings;
        return $categories;
    }



    public function orderHistory($customer_id){
        return Customer::find($customer_id)->orders()
            ->whereHas("shop")
            ->with(['shop:id,name,slug', 'inventories:id,title,slug,product_id', 'status'])
//            ->get();
            ->paginate(100);
    }

  public function orderBatchHistory($customer_id){
        return json_encode(
            OrderBatch::whereCustomerId($customer_id)->orderBy("created_at","DESC")
        ->paginate(20),200);

    }

    public function getRelatedProducts($id){
        $inventory = ListHelper::related_products_by_product($id);
        return $inventory;
    }

    public function getRelatedServices($id){
        $services = ListHelper::related_services_by_product($id);
        return $services;
    }

    public function getServiceCategories(){
        $service_categories = ListHelper::serviceCategoriesForAPI();
        return $service_categories;
    }

    public function getAverageFeedback(Request $request){

        $id = $request->query("id");

        switch($request->query("type")){

            case "shop":
                $average = Shop::find($id)->averageFeedback();
            break;

            case "inventory":
                $average = Inventory::find($id)->averageFeedback();
            break;

            case "product":
                $average = Product::find($id)->averageFeedback();
            break;
        }

        return $average;
    }

    public function getInventoryWishlists(Request $request, $customer_id){

        $wishlistsIds = Wishlist::whereCustomerId($customer_id)
        ->pluck('inventory_id');

        if(empty($wishlistsIds)) return collect([]);

        $listts =  Inventory::whereIn('id', $wishlistsIds)
        // ->available()
        ->inRandomOrder()
        ->with([
            'shop',
            'feedbacks'=> function($f){
                return $f->with(['customer'])->get();
            },
            'images',
            'image:path,imageable_id,imageable_type',
            'product:id,slug',
            'product.image:path,imageable_id,imageable_type'
        ])->paginate(20);

        return ListingResource::collection($listts);
        }

        public static function getProductWishLists($customer_id)
        {
            $productIDs = Wishlist::whereCustomerId($customer_id)->pluck("product_id");

            if(empty($productIDs)) return collect([]);

            $listings = Product::whereIn('id', $productIDs)
            // ->available()
            ->service()
//            ->inRandomOrder()
            ->with([
                'shop',
                'feedbacks'=> function($f){
                    return $f->with(['customer'])->get();
                },
                'images',
                'image:path,imageable_id,imageable_type'
            ])
            ->limit(20)->get();

            return ProductResource::collection($listings);
        }


    public function singleCategoryProducts($category_id)
    {
        $category = Category::find($category_id);
        return $category->load(["listings" => function($listing){
            $a = $listing->available()->with(["wishlist", "shop" => function($shop){
                return $shop->active()->with(["owner" => function($owner){
                    $owner->with("image");
                }]);
            }  ,"image","images", "feedbacks"])->get();
            return ListingResource::collection($a);
        }]);
    }

    public static function shopTrendingProducts($shop_id)
    {

	    $filteredInventoryIDs = Inventory::whereShopId($shop_id)->groupBy("product_id")->distinct()->pluck("id")->toArray();

	    $items = Inventory::whereShopId($shop_id)->whereIn("id", $filteredInventoryIDs)->withCount(['orders' => function($q){
            $q->withArchived();
        }])->orderBy('orders_count', 'desc')
        ->with(['images', 'feedbacks',
        'image', 'shop'=> function($shop){
            return $shop->with(["owner"=> function($o){
                return $o->with("image")->get();
            },"addresses","config","images","feedbacks" => function($f){
                return $f->with(["customer"])->get();
            }])->get();
        }
    ])->limit(15)->get();

    return ListingResource::collection($items);
    }

    public function checkCustomerOrder(Request $request)
    {
        $customer_id = $request->query("customer_id");
        $inventory_id = $request->query("id"); 
        $order_ids =  Order::whereCustomerId($customer_id)->pluck("id")->toArray();

        if( OrderItem::whereIn("order_id", $order_ids)
        ->whereInventoryId($inventory_id)->first()){
        
              return response()->json(["status" => true], 200);
        }else{
            return response()->json(["status" => true], 500);
        };
    }

    public function getProductAttributes($slug){

    	try {
		    $item = Inventory::where( 'slug', $slug )->withCount( 'feedbacks' )->firstOrFail();

		    $item->load( [
			    'product'         => function ( $q ) {
				    $q->select( 'id', 'slug', 'description', 'manufacturer_id' )
				      ->withCount( [
					      'inventories' => function ( $query ) {
						      $query->available();
					      }
				      ] );
			    },
			    'attributeValues' => function ( $q ) {
				    $q->select( 'id', 'attribute_values.attribute_id', 'value', 'color', 'order' )->with( 'attribute:id,name,attribute_type_id,order' );
			    },
			    'feedbacks.customer:id,nice_name,name',
			    'images:path,imageable_id,imageable_type',
		    ] );

		    $variants = ListHelper::variants_of_product( $item, $item->shop_id );

		    $attr_pivots = \DB::table( 'attribute_inventory' )->select( 'attribute_id', 'inventory_id', 'attribute_value_id' )
		                      ->whereIn( 'inventory_id', $variants->pluck( 'id' ) )->get();

		    $item_attrs = $attr_pivots->where( 'inventory_id', $item->id )->pluck( 'attribute_value_id' )->toArray();

		    $attributes = \App\Attribute::select( 'id', 'name', 'attribute_type_id', 'order' )
		                                ->whereIn( 'id', $attr_pivots->pluck( 'attribute_id' ) )
		                                ->with( [
			                                'attributeValues' => function ( $query ) use ( $attr_pivots ) {
				                                $query->whereIn( 'id', $attr_pivots->pluck( 'attribute_value_id' ) )->orderBy( 'order' );
			                                }
		                                ] )->orderBy( 'order' )->get();

		    return response()->json( $attributes, 200 );
	    }catch (\Exception $e){
		    return response()->json( ["message" => $e->getMessage()], 500 );
	    }
    }

    public function getShopsByLocation(Request $request){
    	$shopLists = Shop::with(["owner" => function ($o) {return $o->with("image")->get();
	    }, "addresses", "feedbacks" => function ($f) {return $f->with(["customer"])->get();
	    }, "image", "images", "config"])
		    ->where("place_identifier","LIKE", "%". $request->query("location")."%")->get();

    	return ShopResource::collection($shopLists);
    }


	public function getListingImages(Request $request){
    	$product_id = $request->query("product_id");
    	$listingIds = Inventory::whereProductId($product_id)->pluck('id')->toArray();

//    	$listingImages = Inventory::whereIn("id", $listingIds)->with(["images"])->get();
    	$listingImages = Image::whereIn("imageable_id", $listingIds)->where("imageable_type", "App\Inventory")->get();

		return ImageResource::collection($listingImages);
	}

	private function shopIdExist(){
        return request()->query("shop_id") == '0';
    }

}
