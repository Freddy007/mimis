<?php

namespace App\Http\Controllers\Api;

use App\Address;
use App\Common\Locations\Google\LocationService;
use App\Customer;
use App\Http\Requests\Validations\ProductFeedbackCreateRequest;
use App\Http\Resources\AddressResource;
use App\Inventory;
use App\Order;
use App\Page;
use App\Repositories\Message\MessageRepository;
use App\Shop;
use App\Banner;
use App\Slider;
use App\Product;
use App\State;
use App\Country;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\ShopResource;
use App\Http\Resources\PageResource;
use App\Http\Resources\OfferResource;
use App\Http\Resources\BannerResource;
use App\Http\Resources\SliderResource;
use App\Http\Resources\StateResource;
use App\Http\Resources\CountryResource;
use App\Http\Resources\PackagingResource;
use App\Http\Resources\PaymentMethodResource;
use App\Http\Resources\ShippingResource;
use App\Message;
use App\ShippingZone;
use App\PushyToken;
use App\Wishlist;

class HomeController extends Controller
{
    private $model;

    private $message;

    public function __construct(MessageRepository $message)
    {
        parent::__construct();

        $this->model = trans('app.model.message');

        $this->message = $message;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function sliders()
    {
        $sliders = Slider::whereHas('featuredImage')->get();
        return SliderResource::collection($sliders);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function banners()
    {
        $banners = Banner::all();
        return BannerResource::collection($banners);
    }

    /**
     * Open shop page
     *
     * @param  slug  $slug
     * @return \Illuminate\Http\Response
     */
    public function offers($slug)
    {
        $product = Product::where('slug', $slug)->with(['inventories' => function($q){
                $q->available();
            }, 'inventories.attributeValues.attribute',
            'inventories.feedbacks',
        ])->firstOrFail();

        return new OfferResource($product);
    }

    /**
     * Display the specified resource.
     *
     * @param  str  $slug
     * @return \Illuminate\Http\Response
     */
    public function shop($slug)
    {
        $shop = Shop::where('slug', $slug)->active()
        ->with(["owner" => function($o){
            return $o->with("image")->get();
        }, 'config','feedbacks' => function($q){
            $q->with('customer:id,nice_name,name')->latest()->take(10);
        }])
        ->withCount(['inventories' => function($q){
            $q->available();
        }])->firstOrFail();

        // Check shop maintenance_mode
         if(getShopConfig($shop->id, 'maintenance_mode'))
         if($shop->isDown())
            return response()->json(['message' => trans('app.marketplace_down')], 404);

        return new ShopResource($shop);
    }

    /**
     * Return available packaging options for the specified shop.
     *
     * @param  str  $shop
     * @return \Illuminate\Http\Response
     */
    public function packaging($shop)
    {
        $shop = Shop::where('slug', $shop)->active()->firstOrFail();
        $platformDefaultPackaging = new PackagingResource(getPlatformDefaultPackaging());
        $packagings = PackagingResource::collection($shop->activePackagings);

        return $packagings->prepend($platformDefaultPackaging);
        // return new PackagingResource($platformDefaultPackaging);
        // return PackagingResource::collection($shop->activePackagings);
    }

    /**
     * Return available payment options options for the specified shop.
     *
     * @param  str  $shop
     * @return \Illuminate\Http\Response
     */
    public function paymentOptions($shop)
    {
        $shop = Shop::where('slug', $shop)->active()->firstOrFail();

        $paymentMethods = $shop->paymentMethods;
        $activeManualPaymentMethods = $shop->manualPaymentMethods;
        foreach ($paymentMethods as $key => $payment_provider) {
            $has_config = FALSE;
            switch ($payment_provider->code) {
                case 'stripe':
                  $has_config = $shop->stripe ? TRUE : FALSE;
                  // $info = trans('theme.notify.we_dont_save_card_info');
                  break;

                case 'instamojo':
                  $has_config = $shop->instamojo ? TRUE : FALSE;
                  // $info = trans('theme.notify.you_will_be_redirected_to_instamojo');
                  break;

                case 'authorize-net':
                  $has_config = $shop->authorizeNet ? TRUE : FALSE;
                  // $info = trans('theme.notify.we_dont_save_card_info');
                  break;

                case 'paypal-express':
                  $has_config = $shop->paypalExpress ? TRUE : FALSE;
                  // $info = trans('theme.notify.you_will_be_redirected_to_paypal');
                  break;

                case 'paystack':
                  $has_config = $shop->paystack ? TRUE : FALSE;
                  // $info = trans('theme.notify.you_will_be_redirected_to_paystack');
                  break;

                case 'wire':
                case 'cod':
                    $has_config = in_array($payment_provider->id, $activeManualPaymentMethods->pluck('id')->toArray()) ? TRUE : FALSE;
                    // $temp = $activeManualPaymentMethods->where('id', $payment_provider->id)->first();
                    // $info = $temp ? $temp->pivot->additional_details : '';
                    break;

                default:
                  $has_config = FALSE;
                  break;
            }

            if( ! $has_config )
                $paymentMethods->forget($key);
        }


        return PaymentMethodResource::collection($paymentMethods);
    }

    /**
     * Return available shipping options for the specified shop.
     *
     * @param  str  $shop
     * @return \Illuminate\Http\Response
     */
    public function shipping($shop)
    {
        $shop = Shop::where('slug', $shop)->active()->firstOrFail();
        return PackagingResource::collection($shop->activePackagings);
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function countries()
    {
        $countries = Country::select('id','name','iso_3166_2','iso_3166_3')->get();
        return CountryResource::collection($countries);
    }

    /**
     * Display the specified resource.
     *
     * @param  str  $country
     * @return \Illuminate\Http\Response
     */
    public function states($country)
    {
        $states = State::select('id','name','iso_3166_2')->where('country_id', $country)->get();
        return StateResource::collection($states);
    }

    /**
     * Display the specified resource.
     *
     * @param  str  $slug
     * @return \Illuminate\Http\Response
     */
    public function page($slug)
    {
        $page = Page::where('slug', $slug)->firstOrFail();
        return new PageResource($page);
    }

    public function shopsByType(Request $request, $type="provide-service"){
        if ($request->has("term")){
            $shops = Shop::where("name", "LIKE", "%".$request->term."%")
                ->whereShopType($type)->whereActive(true)->paginate();
            return ShopResource::collection($shops);
        }

       $shops =  Shop::whereShopType($type)->whereActive(true)->get();
       return ShopResource::collection($shops);
    }

//    public function addFeedback()

    public function save_product_feedbacks(Request $request)
    {
        $type        = $request->type;
        $customer_id = $request->customer_id;
        $type_id = $request->type_id;
        $rating = $request->rating;
        $comment = $request->comment;

        $feedback_data["customer_id"] = $customer_id;
        $feedback_data['rating'] = $rating;
        $feedback_data['comment'] = $comment;
        $feedback_data['feedbackable_id'] = $type_id;
        $feedback_data['approved'] = 1;
        $feedback_data['spam'] = 0;

        switch($type){
            case "inventory":
                $feedback_data['feedbackable_type'] = "App\Inventory";
                $inventory = Inventory::find($type_id);
                $inventory->feedbacks()->create($feedback_data);
            break;

            case "shop":
                $feedback_data['feedbackable_type'] = "App\Shop";
                $shop = Shop::find($type_id);
                $shop->feedbacks()->create($feedback_data);
            break;

            case "service":
                $feedback_data['feedbackable_type'] = "App\Product";
                $product = Product::find($type_id);
                $product->feedbacks()->create($feedback_data);
            break;
        }
      
        return response()->json(["message" => "Successful"],200);
    }

    public function saveServicefeedbacks(Request $request)
    {
        $customer_id = $request->customer_id;
        $service_id = $request->service_id;
        $rating = $request->rating;
        $comment = $request->comment;

        $service = Product::find($service_id);

        $feedback_data["customer_id"] = $customer_id;
        $feedback_data['rating'] = $rating;
        $feedback_data['comment'] = $comment;
        $feedback_data['feedbackable_id'] = $service_id;
        $feedback_data['feedbackable_type'] = "App\Product";
        $feedback_data['approved'] = 1;
        $feedback_data['spam'] = 0;

         $service->feedbacks()->create($feedback_data);
        return response()->json(["message" => "Successful"],200);
    }

    public function getProductFeedbacks(Request $request){
        if($request->query("type") == "product"){
            return Inventory::with(["feedbacks" => function($f){
                return $f->with("customer")->first();
            }])->whereId($request->query("id"))->limit(1)->get(["id"]);
        }elseif ($request->query("type") == "shop"){
            return Shop::with(["feedbacks" => function($f){
                return $f->with("customer")->first();
            }])->whereId($request->query("id"))->limit(1)->get(['id']);

        }elseif ($request->query("type") == "service"){
            return Product::with(["feedbacks" => function($f){
                return $f->with("customer")->first();
            }])
            ->whereId($request->query("id"))
            ->whereService(true)
            ->limit(1)->get(['id']);
        }
    }

    public function replies($id){
      $customer = User::find($id);
      return $customer->replies();
    }

    public function setToken(Request $request){
         PushyToken::create([
            "token" => $request->has("token") ? $request->query("token") : null,
            "topic" => $request->has("topic")? $request->query("topic") :null,
            "user_id" => $request->has('user_id') ? $request->query("user_id") : null
        ]);
      }
  
      public function sendMessage(Request $request){

//          $message = $this->message->store($request);

          $shop = Shop::find($request->shop_id);
          Message::create([
              "shop_id"      =>  $request->shop_id,
              'customer_id'  =>  $request->customer_id,
              'subject'      =>  $request->subject,
              'message'      =>  $request->message,
              'token'        => $request->token,
              'user_id'      => $shop->owner_id
          ]);

//          if ($request->hasFile('attachments'))
//              $message->saveAttachments($request->file('attachments'));
      }

      public function allShopMessages(Request $request){
          $shop_id = $request->query("shop_id");
          $customer_id = $request->query("customer_id");

         $messages =  Message::with(["attachments","replies"=>function($r){
             return $r->with("attachments")->orderBy("created_at", "asc")->get();
         }])->whereShopId($shop_id)
          ->whereCustomerId($customer_id)
          ->orderBy("created_at", "asc")
          ->get();

          return $messages;
    }

    public function getAverageFeedback(Request $request){
        $id = $request->query("id");

        switch($request->query("type")){
            case "shop":
                $shop = Shop::find($id);
                $average = $shop->averageFeedback();
                $sum = $shop->sumFeedback();
            break;

            case "inventory":
                $inventory = Inventory::find($id);
                $average = $inventory->averageFeedback();
                $sum =   $inventory ->sumFeedback();
            break;

            case "product":
                $product = Product::find($id);
                $average =  $product->averageFeedback();
                $sum =   $product ->sumFeedback();
            break;
        }

        return json_encode(["average_rating" => $average,"sum" => $sum]) ;
    }

    public function postAddToWishlist(Request $request){
        $customer_id = $request->customer_id;
        $inventory_id = $request->inventory_id != "" ? $request->inventory_id: null;
        $product_id   =  $request->product_id != "" ? $request->product_id : null;

        Wishlist::create([
            "customer_id" => $customer_id,
            "inventory_id" => $inventory_id,
            "product_id"   => $product_id
        ]);

        return response()->json(["success" => true], 200);
     }

	public function postRemoveFromWishlist(Request $request){
		$customer_id = $request->customer_id;
		$inventory_id = $request->inventory_id != "" ? $request->inventory_id: null;
		$product_id   =  $request->product_id != "" ? $request->product_id : null;

		Wishlist::whereProductId($product_id)
		        ->whereCustomerId($customer_id)
		        ->delete();

		return response()->json(["success" => true], 200);
	}

     public function getDeliveryCharge(Request $request){

        $shop = Shop::find($request->query("shop_id"));

        $coordinates = $shop->lat . ",".$shop->lng;

        $type = $request->query("type");

        $place_id = $request->query("place_id");
        $toCoordinates = $request->query("to_coordinates");

         $locationService = new LocationService();

         switch ($type){
            case "points" :
                $charge = $locationService->deliveryChargeByCoordinates($coordinates, utf8_decode($toCoordinates ));
                break;

             case "place_id" :
                 $charge = $locationService->deliveryCharge($coordinates, $place_id );
                 break;

        }

        return json_encode(["charge" => $charge],200 );
     }

     public function shippingRates($shop_id){

         $shipping = ShippingZone::leftJoin("shipping_rates", "shipping_zones.id","=","shipping_rates.shipping_zone_id")->whereShopId($shop_id)
         ->selectRaw("shipping_rates.*")
         ->get();

         ShippingResource::wrap("");
         return ShippingResource::collection( $shipping);
     }

     public function getPlaceDetails(){
	     $locationService = new LocationService();
	    return $locationService->addressByPlaceId("ChIJUakOoNqC3w8Re_kQOy_-dPs");
     }

     public function getAddresses($customer_id){
         $addresses = Address::whereAddressableType(Address::CUSTOMER_ADDRESSABLE_TYPE)
             ->whereAddressableId($customer_id)->get();
         return AddressResource::collection($addresses);
     }

    public function postUpdateAddress(Request $request, $id){
         Address::find($id)->update([
            "address_line_1" => $request->address,
            "city" => $request->city,
            "phone" => $request->phone,
            "latitude" => $request->latitude,
            "longitude" => $request->longitude
            ]
        );
        return response()->json(["message" => "success"],200);
    }

    public function postAddAddress(Request $request){
        $address = Address::create([
                "address_type" => Address::PRIMARY_ADDRESS_TYPE,
                "address_title" => $request->address_title,
                "address_line_1" => $request->address,
                "city" => $request->city,
                "phone" => $request->phone,
                "latitude" => $request->latitude,
                "longitude" => $request->longitude,
                "addressable_id" => $request->customer_id,
                "addressable_type" => Address::CUSTOMER_ADDRESSABLE_TYPE
            ]
        );
        return new AddressResource($address);
    }

    public function postRemoveAddress($id){
        try {
            Address::find($id)->delete();
        } catch (\Exception $e) {
            return response()->json(["message" => "failed"],500);
        }

        return response()->json(["message" => "success"],200);
    }
}
