<?php

namespace App\Http\Controllers\Api;

use App\Category;
use App\CategoryGroup;
use App\CategorySubGroup;
use App\Http\Resources\ProductResource;
use App\Inventory;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\CategoryGroupResource;
use App\Http\Resources\CategorySubGroupResource;
use App\Http\Resources\ListingResource;
use Illuminate\Validation\Rules\In;

class CategoryController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->has("sub_group_id")) {
            $categories = Category::with("image")
                ->whereCategorySubGroupId($request->query('sub_group_id'))
                ->active()
                ->orderBy("created_at", "Desc")
                ->get();

        }elseif ($request->has("home")){

            $popularItemIds = \DB::table("order_items")->groupBy("inventory_id")
                ->selectRaw('sum(quantity) as quantity, inventory_id')
                ->orderBy("quantity","desc")
                ->pluck('inventory_id');

            $productIDs = Inventory::whereIn("id",$popularItemIds)->pluck("product_id");

            $categoryIDs = \DB::table('category_product')
                ->whereIn('product_id', $productIDs)  ->distinct()
                ->pluck('category_id')->toArray();

            if(empty($categoryIDs)) return collect([]);

            $categories = Category::whereIn("id",$categoryIDs)
                ->with("image")
                ->active()
                ->get();
        }else {
            $categories = Category::with("image")
                ->active()
                ->orderBy("created_at","Desc")
                ->get();
        }

        return CategoryResource::collection($categories);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function categoryGroup()
    {
        $categories = CategoryGroup::Where("name","<>", "Services")
                                   ->orwhere("id","<>","10")->orderBy("name","ASC")->active()->get();
        return CategoryGroupResource::collection($categories);
    }


    /**
     * Display a listing of the resource.
     *
     * @return CategorySubGroupResources
     */
    public function categorySubGroup(Request $request)
    {
    	$group_id = $request->query("group_id");
    	if ($group_id == "all") {
		    $categories = CategorySubGroup::where( "category_group_id", "<>", 10 )->with( "group" )->active()->orderBy( "name" )->get();
	    }else{
		    $categories = CategorySubGroup::where( "category_group_id", "<>", 10 )
		                                  ->whereCategoryGroupId($group_id)
		                                  ->with( "group" )->active()->orderBy( "name" )->get();
	    }

        return CategorySubGroupResource::collection($categories);
    }

	public function serviceCategorySubGroup()
	{
		$categories = CategorySubGroup::where("category_group_id","=",10)->with("group")->active()->orderBy("name")->get();
		return CategorySubGroupResource::collection($categories);
	}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function subGroupByCategoryId($category_group_id)
    {
        $categories = CategorySubGroup::whereCategoryGroupId($category_group_id)
            ->active()->get();
        return CategorySubGroupResource::collection($categories);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function productsByCategory($category_id)
    {

        $products = Category::with(["products" =>function($p){
            return $p->with("inventories");
        }])->whereId($category_id)->get();

        return $products;
    }


    public function productsByShopCategory($category_id)
    {
        $shop_id = request()->query("shop_id");
        $cat_array = [$category_id];

        $productIDs = \DB::table('category_product')
                         ->whereIn('category_id', $cat_array)  ->distinct()
                                                    ->pluck('product_id')->toArray();

          $filteredInventoryIDs = Inventory::whereIn("product_id", $productIDs)
                                           ->groupBy("product_id")->distinct()->pluck("id")->toArray();

        if(empty($filteredInventoryIDs)) return collect([]);

        if (empty($shop_id) || $shop_id == ""){
	        $items = Inventory::whereIn('id', $filteredInventoryIDs)->available()->inRandomOrder()
		        ->with([
			        'shop',
			        'feedbacks'=> function($f){
				        return $f->with(['customer'])->get();
			        },
			        'images',
			        'image:path,imageable_id,imageable_type',
			        'product:id,slug',
			        'product.image:path,imageable_id,imageable_type'
		        ])->distinct()->paginate();
        }else{
	        $items = Inventory::whereShopId($shop_id)
		        ->whereIn('id', $filteredInventoryIDs)->available()->inRandomOrder()
		        ->with([
			        'shop',
			        'feedbacks'=> function($f){
				        return $f->with(['customer'])->get();
			        },
			        'images',
			        'image:paths,imageable_id,imageable_type',
			        'product:id,slug',
			        'product.image:path,imageable_id,imageable_type'
		        ])->distinct()->paginate();

        }

        return ListingResource::collection($items);
    }

    public function servicesByShopCategory($category_id)
    {
        $shop_id = request()->query("shop_id");
        $cat_array = [$category_id];

        $productIDs = \DB::table('category_product')
            ->whereIn('category_id', $cat_array)  ->distinct()
            ->pluck('product_id')->toArray();

//        return $productIDs;

//        $filteredIDs = Product::whereIn("id", $productIDs)
//            ->groupBy("id")->distinct()->pluck("id")->toArray();

        if(empty($productIDs)) return collect([]);

        if (empty($shop_id) || $shop_id == ""){
            $items = Product::whereIn('id', $productIDs)
                ->inRandomOrder()
                ->with([
                    'shop',
                    'feedbacks'=> function($f){
                        return $f->with(['customer'])->get();
                    },
                    'images',
                    'image:path,imageable_id,imageable_type'
                ])->distinct()->paginate();
        }else{
            $items = Product::whereShopId($shop_id)
                ->whereIn('id', $productIDs)->available()->inRandomOrder()
                ->with([
                    'shop',
                    'feedbacks'=> function($f){
                        return $f->with(['customer'])->get();
                    },
                    'images',
                    'image:path,imageable_id,imageable_type'
                ])->distinct()->paginate();
        }

//        return ListingResource::collection($items);
        return ProductResource::collection($items);
    }

    public function shopCategories($shop_id) {
		$listProductIDs = \DB::table('inventories')->where('shop_id', $shop_id)
		                                           ->pluck('product_id')->toArray();

		$categoryIDs = \DB::table('category_product')->whereIn('product_id', $listProductIDs)
		                                            ->pluck('category_id')->toArray();
		if(empty($categoryIDs)) return collect([]);

		$categories = Category::whereIn('id', $categoryIDs)->distinct()
		                                              ->paginate(30);

		return CategoryResource::collection($categories);

	}

	public function allCategories() {
		$listProductIDs = \DB::table('inventories')
		                     ->pluck('product_id')->toArray();

		$categoryIDs = \DB::table('category_product')->whereIn('product_id', $listProductIDs)
		                  ->pluck('category_id')->toArray();
		if(empty($categoryIDs)) return collect([]);

		$categories = Category::whereIn('id', $categoryIDs)->distinct()
		                      ->paginate(30);

		return CategoryResource::collection($categories);

	}

	public function shopCategoryProducts($shop_id) {
		$listProductIDs = \DB::table('inventories')->where('shop_id', $shop_id)->groupBy("product_id")->distinct()
		                     ->pluck('product_id')->toArray();

		 $listIDs = \DB::table('inventories')->whereIn("product_id", $listProductIDs)
		                                    ->groupBy("product_id")
		                                    ->distinct()
		                     ->pluck('id')->toArray();

		$categoryIDs = \DB::table('category_product')->whereIn('product_id', $listProductIDs)
//		                                             ->groupBy("product_id")
		                                             ->pluck('category_id')->toArray();

		if(empty($categoryIDs)) return collect([]);

		$categories = Category::whereIn('id', $categoryIDs)
		                      ->with(["listings" => function($listing)use($listIDs){
		                      	return $listing->whereIn("id",$listIDs)->with(["image","images", "shop", "feedbacks"])->distinct()->get();
		                      }])->inRandomOrder()->distinct()->paginate(8);

		return CategoryResource::collection($categories);
	}
}
