<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\ContactUs
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $phone
 * @property string $email
 * @property string $subject
 * @property string $message
 * @property bool|null $read
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ContactUs newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ContactUs newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ContactUs query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ContactUs whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ContactUs whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ContactUs whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ContactUs whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ContactUs whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ContactUs wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ContactUs whereRead($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ContactUs whereSubject($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ContactUs whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ContactUs extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'contact_us';


    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = ['read' => 'boolean'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
                    'name',
                    'phone',
                    'email',
                    'subject',
                    'message',
                    'read',
                 ];
}
