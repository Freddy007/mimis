<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\ConfigInstamojo
 *
 * @property int $shop_id
 * @property string|null $api_key
 * @property string|null $auth_token
 * @property bool|null $sandbox
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ConfigInstamojo newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ConfigInstamojo newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ConfigInstamojo query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ConfigInstamojo whereApiKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ConfigInstamojo whereAuthToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ConfigInstamojo whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ConfigInstamojo whereSandbox($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ConfigInstamojo whereShopId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ConfigInstamojo whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ConfigInstamojo extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'config_instamojo';

    /**
     * The database primary key used by the model.
     *
     * @var string
     */
    protected $primaryKey = 'shop_id';

    /**
     * The attributes that should be casted to boolean types.
     *
     * @var array
     */
    protected $casts = [
        'sandbox' => 'boolean',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
                        'shop_id',
                        'api_key',
                        'auth_token',
                        'sandbox',
					];

    /**
     * Setters.
     */
    public function setApiKeyAttribute($value)
    {
        $this->attributes['api_key'] = trim($value);
    }
    public function setAuthTokenAttribute($value)
    {
        $this->attributes['auth_token'] = trim($value);
    }
    public function setSandboxAttribute($value)
    {
        $this->attributes['sandbox'] = (bool) $value;
    }
}
