<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\ConfigPaystack
 *
 * @property int $shop_id
 * @property string|null $merchant_email
 * @property string|null $public_key
 * @property string|null $secret
 * @property bool|null $sandbox
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ConfigPaystack newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ConfigPaystack newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ConfigPaystack query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ConfigPaystack whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ConfigPaystack whereMerchantEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ConfigPaystack wherePublicKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ConfigPaystack whereSandbox($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ConfigPaystack whereSecret($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ConfigPaystack whereShopId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ConfigPaystack whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ConfigPaystack extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'config_paystacks';

    /**
     * The database primary key used by the model.
     *
     * @var string
     */
    protected $primaryKey = 'shop_id';

    /**
     * The attributes that should be casted to boolean types.
     *
     * @var array
     */
    protected $casts = [
        'sandbox' => 'boolean',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
                        'shop_id',
                        'merchant_email',
                        'public_key',
                        'secret',
                        'sandbox',
					];

    /**
     * Setters.
     */
    public function setMerchantEmailAttribute($value)
    {
        $this->attributes['merchant_email'] = trim($value);
    }
    public function setPublicKeyAttribute($value)
    {
        $this->attributes['public_key'] = trim($value);
    }
    public function setSecretAttribute($value)
    {
        $this->attributes['secret'] = trim($value);
    }
    public function setSandboxAttribute($value)
    {
        $this->attributes['sandbox'] = (bool) $value;
    }
}
