<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\BannerGroup
 *
 * @property string $id
 * @property string $name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Banner[] $banners
 * @property-read int|null $banners_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BannerGroup newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BannerGroup newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BannerGroup query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BannerGroup whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BannerGroup whereName($value)
 * @mixin \Eloquent
 */
class BannerGroup extends Model
{
    protected $table = 'banner_groups';

    /**
     * The primanry key is not incrementing
     *
     * @var boolean
     */
    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
                    'name',
                ];

    /**
     * Group has many banners
     */
    public function banners()
    {
        return $this->hasMany(Banner::class, 'group_id');
    }
}
