<?php

namespace App;

use Parsedown;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Announcement
 *
 * @property string $id
 * @property int $user_id
 * @property string $body
 * @property string|null $action_text
 * @property string|null $action_url
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\User $creator
 * @property-read string $parsed_body
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Announcement newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Announcement newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Announcement query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Announcement whereActionText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Announcement whereActionUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Announcement whereBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Announcement whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Announcement whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Announcement whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Announcement whereUserId($value)
 * @mixin \Eloquent
 */
class Announcement extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'announcements';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The guarded attributes on the model.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['parsed_body'];

    /**
     * Get the user that created the announcement.
     */
    public function creator()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * Get the parsed body of the announcement.
     *
     * @return string
     */
    public function getParsedBodyAttribute()
    {
        return (new Parsedown)->text($this->attributes['body']);
    }
}