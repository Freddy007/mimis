<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Config
 *
 * @property int $shop_id
 * @property string|null $support_phone
 * @property string|null $support_phone_toll_free
 * @property string|null $support_email
 * @property string|null $default_sender_email_address
 * @property string|null $default_email_sender_name
 * @property string|null $return_refund
 * @property string|null $order_number_prefix
 * @property string|null $order_number_suffix
 * @property int|null $default_tax_id
 * @property float|null $order_handling_cost
 * @property int|null $auto_archive_order
 * @property int|null $default_payment_method_id
 * @property int $pagination
 * @property int|null $alert_quantity
 * @property int|null $digital_goods_only
 * @property int|null $default_warehouse_id
 * @property int|null $default_supplier_id
 * @property string|null $default_packaging_ids
 * @property int|null $notify_new_message
 * @property int|null $notify_alert_quantity
 * @property int|null $notify_inventory_out
 * @property int|null $notify_new_order
 * @property int|null $notify_abandoned_checkout
 * @property int|null $notify_new_disput
 * @property bool|null $maintenance_mode
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\ConfigAuthorizeNet|null $authorizeNet
 * @property-read \App\ConfigInstamojo|null $instamojo
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\PaymentMethod[] $manualPaymentMethods
 * @property-read int|null $manual_payment_methods_count
 * @property-read \App\PaymentMethod|null $paymentMethod
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\PaymentMethod[] $paymentMethods
 * @property-read int|null $payment_methods_count
 * @property-read \App\ConfigPaypalExpress|null $paypalExpress
 * @property-read \App\ConfigPaystack|null $paystack
 * @property-read \App\Shop $shop
 * @property-read \App\ConfigStripe|null $stripe
 * @property-read \App\Supplier|null $supplier
 * @property-read \App\Tax|null $tax
 * @property-read \App\Warehouse|null $warehouse
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Config down()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Config live()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Config newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Config newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Config query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Config whereAlertQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Config whereAutoArchiveOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Config whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Config whereDefaultEmailSenderName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Config whereDefaultPackagingIds($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Config whereDefaultPaymentMethodId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Config whereDefaultSenderEmailAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Config whereDefaultSupplierId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Config whereDefaultTaxId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Config whereDefaultWarehouseId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Config whereDigitalGoodsOnly($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Config whereMaintenanceMode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Config whereNotifyAbandonedCheckout($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Config whereNotifyAlertQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Config whereNotifyInventoryOut($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Config whereNotifyNewDisput($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Config whereNotifyNewMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Config whereNotifyNewOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Config whereOrderHandlingCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Config whereOrderNumberPrefix($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Config whereOrderNumberSuffix($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Config wherePagination($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Config whereReturnRefund($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Config whereShopId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Config whereSupportEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Config whereSupportPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Config whereSupportPhoneTollFree($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Config whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Config extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'configs';

    /**
     * The database primary key used by the model.
     *
     * @var string
     */
    protected $primaryKey = 'shop_id';

    /**
     * The primanry key is not incrementing
     *
     * @var boolean
     */
    public $incrementing = false;

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'maintenance_mode' => 'boolean',
    ];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Get the shop.
     */
    public function shop()
    {
        return $this->belongsTo(Shop::class);
    }

    /**
     * Get the tax.
     */
    public function tax()
    {
        return $this->belongsTo(Tax::class, 'default_tax_id');
    }

    /**
     * Get the default payment method.
     */
    public function paymentMethod()
    {
        return $this->belongsTo(PaymentMethod::class, 'default_payment_method_id');
    }

    /**
     * Get the paymentMethods for the shop.
     */
    public function paymentMethods()
    {
        return $this->belongsToMany(PaymentMethod::class, 'shop_payment_methods', 'shop_id', 'payment_method_id')->withTimestamps();
    }

   /**
     * Get the manualPaymentMethods for the shop.
     */
    public function manualPaymentMethods()
    {
        return $this->belongsToMany(PaymentMethod::class, 'config_manual_payments', 'shop_id', 'payment_method_id')
        ->withPivot('additional_details', 'payment_instructions')->withTimestamps();
    }

    /**
     * Get the stripe for the shop.
     */
    public function stripe()
    {
        return $this->hasOne(ConfigStripe::class, 'shop_id');
    }

    /**
     * Get the authorizeNet for the shop.
     */
    public function authorizeNet()
    {
        return $this->hasOne(ConfigAuthorizeNet::class, 'shop_id');
    }

    /**
     * Get the paypalExpress for the shop.
     */
    public function paypalExpress()
    {
        return $this->hasOne(ConfigPaypalExpress::class, 'shop_id');
    }

    /**
     * Get the instamojo for the shop.
     */
    public function instamojo()
    {
        return $this->hasOne(ConfigInstamojo::class, 'shop_id');
    }

    /**
     * Get the paystack for the shop.
     */
    public function paystack()
    {
        return $this->hasOne(ConfigPaystack::class, 'shop_id');
    }

    /**
     * Get the supplier.
     */
    public function supplier()
    {
        return $this->belongsTo(Supplier::class, 'default_supplier_id');
    }

    /**
     * Get the warehouse.
     */
    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class, 'default_warehouse_id');
    }

    /**
     * Setters
     */
    public function setDefaultPackagingIdsAttribute($value)
    {
        $this->attributes['default_packaging_ids'] = serialize($value);
    }

    /**
     * Getters
     */
    public function getDefaultPackagingIdsAttribute($value)
    {
        return unserialize($value);
    }

    /**
     * Scope a query to only include active shops.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeLive($query)
    {
        return $query->where('maintenance_mode', 0)->orWhereNull('maintenance_mode');
    }

    /**
     * Scope a query to only include shops thats are down for maintainance.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeDown($query)
    {
        return $query->where('maintenance_mode', 1);
    }
}
