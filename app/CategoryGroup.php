<?php

namespace App;

use App\Common\Imageable;
use App\Common\CascadeSoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\CategoryGroup
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property string|null $description
 * @property string|null $icon
 * @property int $active
 * @property int|null $order
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Image|null $bannerbg
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Category[] $categories
 * @property-read int|null $categories_count
 * @property-read \App\Image|null $featuredImage
 * @property-read \App\Image|null $image
 * @property-read \App\Image|null $logo
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\CategorySubGroup[] $subGroups
 * @property-read int|null $sub_groups_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CategoryGroup active()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CategoryGroup newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CategoryGroup newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\CategoryGroup onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CategoryGroup query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CategoryGroup whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CategoryGroup whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CategoryGroup whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CategoryGroup whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CategoryGroup whereIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CategoryGroup whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CategoryGroup whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CategoryGroup whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CategoryGroup whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CategoryGroup whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\CategoryGroup withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\CategoryGroup withoutTrashed()
 * @mixin \Eloquent
 */
class CategoryGroup extends Model
{
    use SoftDeletes, CascadeSoftDeletes, Imageable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description', 'slug', 'icon', 'order', 'active'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Cascade Soft Deletes Relationships
     *
     * @var array
     */
    protected $cascadeDeletes = ['subGroups'];

    /**
     * Get the subGroups associated with the CategoryGroup.
    */
    public function subGroups()
    {
        return $this->hasMany(CategorySubGroup::class, 'category_group_id');
    }

    /**
     * Get the categories associated with the CategoryGroup.
    */
    public function categories()
    {
        return $this->hasManyThrough(Category::class, CategorySubGroup::class,
            'category_group_id', // Foreign key on CategorySubGroup table...
            'category_sub_group_id', // Foreign key on Category table...
            'id', // Local key on CategoryGroup table...
            'id' // Local key on CategorySubGroup table...
        );
    }

    /**
     * Setters
     */
    public function setOrderAttribute($value)
    {
        $this->attributes['order'] = $value ?: 100;
    }

    /**
     * Scope a query to only include active categoryGroups.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

}
