<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\AttributeType
 *
 * @property int $id
 * @property string $type
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Attribute[] $attributes
 * @property-read int|null $attributes_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AttributeType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AttributeType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AttributeType query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AttributeType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AttributeType whereType($value)
 * @mixin \Eloquent
 */
class AttributeType extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'attribute_types';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * AttributeType has many Attributes
     */
    public function attributes()
    {
        return $this->hasMany(Attribute::class);
    }

}
