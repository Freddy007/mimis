<?php

namespace App\Jobs;

use App\Cart;
use App\Common\PushyAPI;
use App\Events\Order\OrderCreated;
use App\Http\Resources\OrderResource;
use App\Order;
use App\PaymentMethod;
use App\User;
use DB;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class ProcessUnPaidOrders
{
    use Dispatchable;

    protected $request;
    protected $cartIds;
    protected $paymentStatus;
    protected $token;

    public function __construct(Request $request, $cartIds)
    {
        $this->request = $request;
        $this->cartIds = $cartIds;
//        $this->paymentStatus = $paymentStatus;
//        $this->token = $token;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $orders = new Collection();

        foreach ($this->cartIds as $cart_id){
            $orders->push( $this->startOrderProcess($this->request, $cart_id, $this->paymentStatus));
        }

    }

    private function startOrderProcess(Request $request, $cart_id,$payment_status=NULL){

        $cart = Cart::whereId($cart_id)->first();

        if( !crosscheckCartOwnership($request, $cart) )
            return response()->json(['message' => trans('theme.notify.please_login_to_checkout')], 404);

        // Update the cart
        $cart->shipping_rate_id = $request->shipping_option_id;
        $cart->packaging_id = $request->packaging_id;
        $cart->payment_method_id = $payment_status != NULL ? PaymentMethod::TYPE_FLUTTER : $request->payment_method_id;
        $cart->customer_id       = $request->customer_id;

//        if($request->shipping_option_id)
//            $cart->shipping = getShippingingCost($request->shipping_option_id);

//        if($request->packaging_id)
//            $cart->packaging = getPackagingCost($request->packaging_id);

        $cart->save();

        $cart = crosscheckAndUpdateOldCartInfo($request, $cart);

        // Start transaction!
        DB::beginTransaction();
        try {
            // Create the order from the cart
            $paymentStatus = $payment_status!=NULL ? $payment_status : Order::PAYMENT_STATUS_UNPAID;
            $order = saveOrderFromCart($request, $cart, $paymentStatus);

        } catch(\Exception $e){
            \Log::error($e);        // Log the error

            // rollback the transaction and log the error
            DB::rollback();

            return response()->json(["error" => $e->getMessage(),trans('theme.notify.order_creation_failed')], 500);
        }

        // Everything is fine. Now commit the transaction
        DB::commit();

        $cart->forceDelete();   // Delete the cart

        event(new OrderCreated($order));   // Trigger the Event

//        $ new OrderResource($order);
        return new OrderResource($order);
    }

}
