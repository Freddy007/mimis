<?php

namespace App\Repositories\CategorySubGroup;

use App\CategorySubGroup;
use App\Repositories\BaseRepository;
use App\Repositories\EloquentRepository;
use Illuminate\Http\Request;

class EloquentCategorySubGroup extends EloquentRepository implements BaseRepository, CategorySubGroupRepository
{
	protected $model;

	public function __construct(CategorySubGroup $categorySubGroup)
	{
		$this->model = $categorySubGroup;
	}

    public function all()
    {
        return $this->model->with('group:id,name,deleted_at')->withCount('categories')->get();
    }

    public function trashOnly()
    {
        return $this->model->with('group:id,name,deleted_at')->onlyTrashed()->get();
    }

	public function store(Request $request)
	{
		$subGrp = parent::store($request);

		if ($request->hasFile('image'))
			$subGrp->saveImage($request->file('image'));

		return $subGrp;
	}

	public function update(Request $request, $id)
	{
		$subGrp = parent::update($request, $id);

		if ($request->hasFile('image') || ($request->input('delete_image') == 1))
			$subGrp->deleteImage();

		if ($request->hasFile('image'))
			$subGrp->saveImage($request->file('image'));

		return $subGrp;
	}
}