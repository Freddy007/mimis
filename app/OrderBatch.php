<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderBatch extends Model
{
    //
    protected $table = "order_batches";

    protected $guarded = [];

    protected $casts = [
        'order_items' => 'array',
    ];
}
