<?php

namespace App;

use App\Common\CascadeSoftDeletes;
use App\Common\Imageable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\CategorySubGroup
 *
 * @property int $id
 * @property int $category_group_id
 * @property string $name
 * @property string $slug
 * @property string|null $description
 * @property int $active
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Category[] $categories
 * @property-read int|null $categories_count
 * @property-read \App\CategoryGroup $group
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CategorySubGroup active()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CategorySubGroup newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CategorySubGroup newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\CategorySubGroup onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CategorySubGroup query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CategorySubGroup whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CategorySubGroup whereCategoryGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CategorySubGroup whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CategorySubGroup whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CategorySubGroup whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CategorySubGroup whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CategorySubGroup whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CategorySubGroup whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CategorySubGroup whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\CategorySubGroup withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\CategorySubGroup withoutTrashed()
 * @mixin \Eloquent
 */
class CategorySubGroup extends Model
{
    use SoftDeletes, CascadeSoftDeletes,Imageable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'category_group_id', 'slug', 'description', 'active'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Cascade Soft Deletes Relationships
     *
     * @var array
     */
    protected $cascadeDeletes = ['categories'];

    /**
     * Get the categoryGroup that owns the SubGroup.
     */
    public function group()
    {
        return $this->belongsTo(CategoryGroup::class, 'category_group_id')->withTrashed();
    }

    /**
     * Get the categories for the CategorySubGroup.
     */
    public function categories()
    {
        return $this->hasMany(Category::class, 'category_sub_group_id')->orderBy('name', 'asc');
        // return $this->belongsToMany(Category::class)->orderBy('name', 'asc')->withTimestamps();
    }

    // /**
    //  * Get all listings for the category.
    //  */
    // public function getListingsAttribute()
    // {
    //     return \DB::table('inventories')
    //     ->join('category_product', 'inventories.product_id', '=', 'category_product.product_id')
    //     ->select('inventories.*', 'category_product.category_id')
    //     ->where('category_product.category_id', '=', $this->id)->get();

    //     // return $this->belongsToMany(Inventory::class, 'category_product', null, 'product_id', null, 'product_id');
    // }

    /**
     * Scope a query to only include active categorySubGroups.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }
}
