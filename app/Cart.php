<?php

namespace App;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Cart
 *
 * @property int $id
 * @property int|null $shop_id
 * @property int|null $customer_id
 * @property string|null $ip_address
 * @property int|null $ship_to
 * @property int|null $shipping_zone_id
 * @property int|null $shipping_rate_id
 * @property int|null $packaging_id
 * @property int $item_count
 * @property int $quantity
 * @property float|null $total
 * @property float|null $discount
 * @property float|null $shipping
 * @property float|null $packaging
 * @property float|null $handling
 * @property float|null $taxes
 * @property float|null $grand_total
 * @property float|null $taxrate
 * @property float|null $shipping_weight
 * @property int|null $billing_address
 * @property int|null $shipping_address
 * @property int|null $coupon_id
 * @property int $payment_status
 * @property int|null $payment_method_id
 * @property string|null $message_to_customer
 * @property string|null $admin_note
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Address|null $billingAddress
 * @property-read \App\Coupon|null $coupon
 * @property-read \App\Customer|null $customer
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Inventory[] $inventories
 * @property-read int|null $inventories_count
 * @property-read \App\PaymentMethod|null $paymentMethod
 * @property-read \App\Country|null $shipTo
 * @property-read \App\Address|null $shippingAddress
 * @property-read \App\Packaging|null $shippingPackage
 * @property-read \App\ShippingRate|null $shippingRate
 * @property-read \App\ShippingZone|null $shippingZone
 * @property-read \App\Shop|null $shop
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Cart mine()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Cart newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Cart newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Cart onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Cart query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Cart whereAdminNote($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Cart whereBillingAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Cart whereCouponId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Cart whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Cart whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Cart whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Cart whereDiscount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Cart whereGrandTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Cart whereHandling($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Cart whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Cart whereIpAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Cart whereItemCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Cart whereMessageToCustomer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Cart wherePackaging($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Cart wherePackagingId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Cart wherePaymentMethodId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Cart wherePaymentStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Cart whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Cart whereShipTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Cart whereShipping($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Cart whereShippingAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Cart whereShippingRateId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Cart whereShippingWeight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Cart whereShippingZoneId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Cart whereShopId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Cart whereTaxes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Cart whereTaxrate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Cart whereTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Cart whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Cart withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Cart withoutTrashed()
 * @mixin \Eloquent
 */
class Cart extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'carts';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Load item count with cart
     *
     * @var array
     */
    protected $withCount = ['inventories'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
                        'shop_id',
                        'customer_id',
                        'ip_address',
                        'ship_to',
                        'shipping_zone_id',
                        'shipping_rate_id',
                        'packaging_id',
                        'taxrate',
                        'item_count',
                        'quantity',
                        'total',
                        'discount',
                        'shipping',
                        'packaging',
                        'handling',
                        'taxes',
                        'grand_total',
                        'shipping_weight',
                        'shipping_address',
                        'billing_address',
                        'coupon_id',
                        'payment_method_id',
                        'payment_status',
                        'message_to_customer',
                        'admin_note',
                    ];

    /**
     * Get the country associated with the order.
     */
    public function shipTo()
    {
        return $this->belongsTo(Country::class, 'ship_to');
    }

    /**
     * Get the customer associated with the cart.
     */
    public function customer()
    {
        return $this->belongsTo(Customer::class)->withDefault([
            'name' => trans('app.guest_customer'),
        ]);
    }

    /**
     * Get the shop associated with the cart.
     */
    public function shop()
    {
        return $this->belongsTo(Shop::class)->withDefault();
    }

    /**
     * Fetch billing address
     *
     * @return Address or null
     */
    public function billingAddress()
    {
        return $this->belongsTo(Address::class, 'billing_address');
    }

    /**
     * Fetch billing address
     *
     * @return Address or null
     */
    public function shippingAddress()
    {
        return $this->belongsTo(Address::class, 'shipping_address');
    }

    /**
     * Get the shippingZone for the order.
     */
    public function shippingZone()
    {
        return $this->belongsTo(ShippingZone::class, 'shipping_zone_id');
    }

    /**
     * Get the shippingRate for the order.
     */
    public function shippingRate()
    {
        return $this->belongsTo(ShippingRate::class, 'shipping_rate_id');
    }

    /**
     * Get the packaging for the order.
     */
    public function shippingPackage()
    {
        return $this->belongsTo(Packaging::class, 'packaging_id');
    }

    /**
     * Get the carrier associated with the cart.
     */
    public function carrier()
    {
        return optional($this->shippingRate)->carrier();
    }

    /**
     * Get the coupon associated with the order.
     */
    public function coupon()
    {
        return $this->belongsTo(Coupon::class);
    }

    /**
     * Get the inventories for the product.
     */
    public function inventories()
    {
        return $this->belongsToMany(Inventory::class, 'cart_items')
        ->withPivot('item_description', 'quantity', 'unit_price','other')->withTimestamps();
    }

    /**bv
     * Get the paymentMethod for the order.
     */
    public function paymentMethod()
    {
        return $this->belongsTo(PaymentMethod::class);
    }

    /**
     * Return shipping cost with handling fee
     *
     * @return number
     */
    public function get_shipping_cost()
    {
        return $this->is_free_shipping() ? 0 : $this->shipping + $this->handling;
    }

    /**
     * Return grand tolal
     *
     * @return number
     */
    public function grand_total()
    {
        if($this->is_free_shipping())
            return ($this->total + $this->taxes + $this->packaging) - $this->discount;

        return ($this->total + $this->handling + $this->taxes + $this->shipping + $this->packaging) - $this->discount;
    }

    /**
     * Check if the cart eligable for free shipping
     *
     * @return bool
     */
    public function is_free_shipping()
    {
        if( ! $this->shipping_rate_id )
            return TRUE;

        return FALSE;
    }

    /**
     * Setters
     */
    public function setDiscountAttribute($value){
        $this->attributes['discount'] = $value ?? Null;
    }
    public function setShippingAddressAttribute($value){
        $this->attributes['shipping_address'] = is_numeric($value) ? $value : Null;
    }
    public function setBillingAddressAttribute($value){
        $this->attributes['billing_address'] = is_numeric($value) ? $value : Null;
    }

    /**
     * Scope a query to only include records from the users shop.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeMine($query)
    {
        return $query->where('shop_id', Auth::user()->merchantId());
    }
}
