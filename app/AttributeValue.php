<?php

namespace App;

use App\Common\Imageable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\AttributeValue
 *
 * @property int $id
 * @property int|null $shop_id
 * @property string|null $value
 * @property string|null $color
 * @property int $attribute_id
 * @property int|null $order
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Attribute $attribute
 * @property-read \App\Image|null $bannerbg
 * @property-read \App\Image|null $featuredImage
 * @property-read \App\Image|null $image
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Inventory[] $inventories
 * @property-read int|null $inventories_count
 * @property-read \App\Image|null $logo
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AttributeValue mine()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AttributeValue newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AttributeValue newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\AttributeValue onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AttributeValue query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AttributeValue whereAttributeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AttributeValue whereColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AttributeValue whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AttributeValue whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AttributeValue whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AttributeValue whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AttributeValue whereShopId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AttributeValue whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AttributeValue whereValue($value)
 * @method static \Illuminate\Database\Query\Builder|\App\AttributeValue withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\AttributeValue withoutTrashed()
 * @mixin \Eloquent
 */
class AttributeValue extends Model
{
    use SoftDeletes, Imageable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'attribute_values';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
                    'shop_id',
                    'value',
                    'color',
                    'attribute_id',
                    'order',
                ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Get the attribute for the AttributeValue.
     */
    public function attribute()
    {
        return $this->belongsTo(Attribute::class);
    }

    /**
     * Get the inventories for the supplier.
     */
    public function inventories()
    {
        return $this->belongsToMany(Inventory::class, 'attribute_inventory')
                    ->withPivot('attribute_id')
                    ->withTimestamps();
    }
    /**
     * Scope a query to only include records from the users shop.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeMine($query)
    {
        return $query->where('shop_id', Auth::user()->merchantId());
    }
}
