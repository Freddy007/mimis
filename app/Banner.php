<?php

namespace App;

use App\Common\Imageable;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Banner
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $description
 * @property string|null $link
 * @property string|null $link_label
 * @property string|null $bg_color
 * @property string|null $group_id
 * @property int|null $columns
 * @property int|null $order
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Image|null $bannerbg
 * @property-read \App\Image|null $featuredImage
 * @property-read \App\BannerGroup|null $group
 * @property-read \App\Image|null $image
 * @property-read \App\Image|null $logo
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Banner newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Banner newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Banner query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Banner whereBgColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Banner whereColumns($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Banner whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Banner whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Banner whereGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Banner whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Banner whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Banner whereLinkLabel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Banner whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Banner whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Banner whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Banner extends Model
{
    use Imageable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'banners';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
                    'title',
                    'description',
                    'link',
                    'link_label',
                    'bg_color',
                    'group_id',
                    'columns',
                    'order',
                ];

    /**
     * Get the group for the banner.
     */
    public function group()
    {
        return $this->belongsTo(BannerGroup::class);
    }

    /**
     * Setters
     */
    public function setOrderAttribute($value)
    {
        $this->attributes['order'] = $value ?: 100;
    }

    // public function setOptionsAttribute($value)
    // {
    //     $this->attributes['options'] = serialize($value);
    // }

    // /**
    //  * Getters
    //  */
    // public function getOptionsAttribute($value)
    // {
    //     return unserialize($value);
    // }

}
