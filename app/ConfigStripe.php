<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\ConfigStripe
 *
 * @property int $shop_id
 * @property string|null $stripe_user_id
 * @property string|null $publishable_key
 * @property string|null $refresh_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ConfigStripe newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ConfigStripe newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ConfigStripe query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ConfigStripe whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ConfigStripe wherePublishableKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ConfigStripe whereRefreshToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ConfigStripe whereShopId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ConfigStripe whereStripeUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ConfigStripe whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ConfigStripe extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'config_stripes';

    /**
     * The database primary key used by the model.
     *
     * @var string
     */
    protected $primaryKey = 'shop_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
                        'shop_id',
                        'stripe_user_id',
                        'publishable_key',
                        'refresh_token',
					];

    /**
     * Return Stripe client id from config
     */
    // public static function getClientId()
    // {
    //     return config('services.stripe.client_id');
    // }
}
