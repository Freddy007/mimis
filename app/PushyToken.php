<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PushyToken extends Model
{
    
        /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pushy_tokens';

    protected $fillable = [
        'id',
        'token',
        'topic',
        'user_id'
    ];
}
