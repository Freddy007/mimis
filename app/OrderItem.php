<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
Use Illuminate\Database\Eloquent\SoftDeletes;

class OrderItem extends Model
{
    //
//    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */protected $guarded = [];

     public function orders(){
         return $this->belongsTo(Order::class);
     }

    public function inventories(){
        return $this->belongsTo(Inventory::class);
    }
}
