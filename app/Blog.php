<?php

namespace App;

use Carbon\Carbon;
use App\Common\Taggable;
use App\Common\Imageable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Blog
 *
 * @property int $id
 * @property string $title
 * @property string $slug
 * @property string|null $excerpt
 * @property string|null $content
 * @property int $user_id
 * @property int $status
 * @property int $approved
 * @property \Illuminate\Support\Carbon|null $published_at
 * @property int $likes
 * @property int $dislikes
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\User $author
 * @property-read \App\Image|null $bannerbg
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\BlogComment[] $comments
 * @property-read int|null $comments_count
 * @property-read \App\Image|null $featuredImage
 * @property-read array $tag_list
 * @property-read \App\Image|null $image
 * @property-read \App\Image|null $logo
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Tag[] $tags
 * @property-read int|null $tags_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Blog newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Blog newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Blog onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Blog popular()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Blog published()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Blog query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Blog recent()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Blog whereApproved($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Blog whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Blog whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Blog whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Blog whereDislikes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Blog whereExcerpt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Blog whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Blog whereLikes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Blog wherePublishedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Blog whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Blog whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Blog whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Blog whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Blog whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Blog withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Blog withoutTrashed()
 * @mixin \Eloquent
 */
class Blog extends Model
{
    use SoftDeletes, Imageable, Taggable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'blogs';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at', 'published_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
                    'title',
                    'slug',
                    'excerpt',
                    'content',
                    'published_at',
                    'user_id',
                    'status'
                ];

    /**
     * Get the author associated with the blog post.
     */
    public function author()
    {
        return $this->belongsTo(User::class, 'user_id')->withDefault();
    }

	/**
     * Get the comments for the blog post.
     */
    public function comments()
    {
        return $this->hasMany(BlogComment::class);
    }

    /**
     * Scope a query to only include published blogs.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePublished($query)
    {
        return $query->whereNotNull('published_at')
        ->where([
            ['status', '=', 1],
            ['approved', '=', 1],
            ['published_at', '<=', Carbon::now()]
        ]);
    }

    /**
     * Scope a query to most polular blogs.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePopular($query)
    {
        return $query->orderBy('likes', 'DESC');
    }


    /**
     * Scope a query to most polular blogs.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeRecent($query)
    {
        return $query->orderBy('published_at', 'DESC');
    }

    /**
     * Set tag time formate for the post.
     *
     * @return array
     */
    public function setPublishedAtAttribute($value)
    {
        if($value) $this->attributes['published_at'] = Carbon::createFromFormat('Y-m-d h:i a', $value);
        // $this->attributes['published_at'] = $value ? date("Y-m-d H:i:s", strtotime($value)) : null;
    }

}
