<?php

namespace App;

use App\Common\Imageable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Carrier
 *
 * @property int $id
 * @property int|null $shop_id
 * @property int|null $tax_id
 * @property string $name
 * @property string|null $email
 * @property string|null $phone
 * @property string|null $tracking_url
 * @property int $active
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Image|null $bannerbg
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Cart[] $carts
 * @property-read int|null $carts_count
 * @property-read \App\Image|null $featuredImage
 * @property-read \App\Image|null $image
 * @property-read \App\Image|null $logo
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Order[] $orders
 * @property-read int|null $orders_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\ShippingRate[] $shippingRates
 * @property-read int|null $shipping_rates_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\ShippingZone[] $shippingZones
 * @property-read int|null $shipping_zones_count
 * @property-read \App\Shop|null $shop
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Carrier active()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Carrier mine()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Carrier newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Carrier newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Carrier onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Carrier query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Carrier whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Carrier whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Carrier whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Carrier whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Carrier whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Carrier whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Carrier wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Carrier whereShopId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Carrier whereTaxId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Carrier whereTrackingUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Carrier whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Carrier withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Carrier withoutTrashed()
 * @mixin \Eloquent
 */
class Carrier extends Model
{
    use SoftDeletes, Imageable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'carriers';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
                    'shop_id',
                    'name',
                    'email',
                    'phone',
                    'tracking_url',
                    'active'
                 ];

    /**
     * Get all of the shippingRates for the country.
     */
    public function shippingRates()
    {
        return $this->hasMany(ShippingRate::class);
    }

    /**
     * Get all of the shippingZones for the country.
     */
    public function shippingZones()
    {
        return $this->belongsToMany(ShippingZone::class, 'shipping_rates');
    }

    /**
     * Get the Shop associated with the carrier.
     */
    public function shop()
    {
        return $this->belongsTo(Shop::class);
    }

    /**
     * Get the carts for the carrier.
     */
    public function carts()
    {
        return $this->hasMany(Cart::class);
    }

    /**
     * Get the orders for the carrier.
     */
    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    /**
     * Scope a query to only include active records.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

    /**
     * Scope a query to only include records from the users shop.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeMine($query)
    {
        return $query->where('shop_id', Auth::user()->merchantId());
    }
}