<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['namespace' => 'Api'], function(){
	Route::get('sliders', 'HomeController@sliders');
	Route::get('slider-images', 'ListingController@allSliderImages');
	Route::get('banners', 'HomeController@banners');
	Route::get('categories', 'CategoryController@index');
	Route::get('all-categories', 'CategoryController@allCategories');
	Route::get('category-grps', 'CategoryController@categoryGroup');
//	Route::get('categories/{subgrp_id}', 'CategoryController@categoryBySubGroup');
	Route::get('category-subgrps', 'CategoryController@categorySubGroup');

	Route::get('service-category-subgrps', 'CategoryController@serviceCategorySubGroup');

	Route::get('shop-categories/{shop_id}', 'CategoryController@shopCategories');

	Route::get('shop-category-products/{shop_id}', 'CategoryController@shopCategoryProducts');

	Route::get('category-group-subgroup/{category_group_id}', 'CategoryController@subGroupByCategoryId');

	Route::get('products-by-category/{category_id}', 'CategoryController@productsByCategory');
	
	Route::get('products-by-shop-category/{category_id}', 'CategoryController@productsByShopCategory');

	Route::get('services-by-shop-category/{category_id}', 'CategoryController@servicesByShopCategory');

    Route::get('countries', 'HomeController@countries');
	Route::get('states/{country}', 'HomeController@states');
	Route::get('page/{slug}', 'HomeController@page');
	Route::get('shop/{slug}', 'HomeController@shop');
	Route::get('packaging/{shop}', 'HomeController@packaging');

	Route::get('shipping-rates/{shop}', 'HomeController@shippingRates');

	Route::get('paymentOptions/{shop}', 'HomeController@paymentOptions');
	Route::get('offers/{slug}', 'HomeController@offers');

	Route::get('shops-by-type/{type?}', 'HomeController@shopsByType');

	Route::get('listings/{list}', 'ListingController@index');

	Route::get('listing/{slug}', 'ListingController@item');

	Route::get('single-shop-listing/{shop_id}', 'ListingController@singleShopListing');
	
	Route::get('shop-trending-products/{shop_id}', 'ListingController@shopTrendingProducts');	

	Route::get('search', 'ListingController@search');

	Route::get('offers', 'ListingController@Offers');

	Route::get('shop/{slug}/listings', 'ListingController@shop');

	Route::get('all-shops-listings', 'ListingController@allShopsListings');

	Route::get('all-listings', 'ListingController@allListings');

	Route::get('product-attributes/{slug}', 'ListingController@getProductAttributes');

	Route::get('category/{slug}', 'ListingController@category');

	Route::get('brand/{slug}', 'ListingController@brand');

	Route::get('shops', 'ListingController@shops');

	Route::get('product-listings/{list?}', 'ListingController@productListings');

    Route::get('service-listings/{limit?}', 'ListingController@serviceListings');

    Route::get('image-listings/{productId}', 'ListingController@listingImages');

    Route::get('listing-images', 'ListingController@getListingImages');

    Route::get('all-image-listings/{productId}', 'ListingController@getImageListings');

    Route::get('all-Image-listings/{productId}', 'ListingController@getImageListings');

    Route::get('shop-image-listings/{shopId}', 'ListingController@getShopImageListings');

    Route::get('category-products/{shopId}', 'ListingController@categoryProducts');

    Route::get('single-category-products/{category_id}', 'ListingController@singleCategoryProducts');

    Route::get('category-products', 'ListingController@categoryProducts');

    Route::get('all-category-products', 'ListingController@allCategoryProducts');

    Route::get('shop-categories', 'ListingController@shopCategories');

    Route::get('shops-by-category/{slug}', 'ListingController@shopsByCategory');

    Route::get('single-category/{categoryId}', 'ListingController@categoryProductsById');

    Route::get('category-shops/{id}', 'ListingController@categoryShops');

	Route::get('replies/{id}', 'HomeController@replies');

	Route::get('all-shop-messages', 'HomeController@allShopMessages');

	Route::post('send-message', 'HomeController@sendMessage');
	
	Route::get('set-token', 'HomeController@setToken');

	Route::post('add-product-feedback', 'HomeController@save_product_feedbacks');

	Route::get('feedbacks', 'HomeController@getProductFeedbacks');
	
	Route::get('average-feedbacks', 'HomeController@getAverageFeedback');

	//Orders
	
	Route::get('check-customer-order', 'ListingController@checkCustomerOrder');

	Route::get('order-history/{customer_id}', 'ListingController@orderHistory');

    Route::get('order-batch-history/{customer_id}', 'ListingController@orderBatchHistory');
	
	Route::post('add-to-wish-list', 'HomeController@postAddToWishlist');

	Route::post('remove-from-wish-list', 'HomeController@postRemoveFromWishlist');

	Route::get('inventory-wishlists/{customer_id}', 'ListingController@getInventoryWishlists');

	Route::get('product-wishlists/{customer_id}', 'ListingController@getProductWishLists');

	Route::get('related-products/{id}', 'ListingController@getRelatedProducts');
	
	Route::get('related-services/{id}', 'ListingController@getRelatedServices');

	Route::get('service-categories', 'ListingController@getServiceCategories');

	Route::get('shops-by-location', 'ListingController@getShopsByLocation');

	Route::get('delivery-charge', 'HomeController@getDeliveryCharge');

	Route::get('place-details', 'HomeController@getPlaceDetails');

	Route::get('addresses/{customer_id}', 'HomeController@getAddresses');

	Route::post('update-address/{id}', 'HomeController@postUpdateAddress');

	Route::post('add-address', 'HomeController@postAddAddress');

	Route::post('remove-address/{id}', 'HomeController@postRemoveAddress');

    // CART
	Route::group(['middleware' => 'ajax'], function(){
		Route::post('addToCart/{slug}', 'CartController@addToCart');
		Route::post('addToCart-mobile/{slug}', 'CartController@addToCartForMobile');
		Route::post('cart/removeItem', 'CartController@remove');
	});

	// Route::group(['middleware' => 'auth:customer'], function(){
		Route::post('cart/{cart}/applyCoupon', 'CartController@validateCoupon');
		// Route::post('cart/{cart}/applyCoupon', 'CartController@validateCoupon')->middleware(['ajax']);
	// });

	Route::get('carts', 'CartController@index');
	// Route::put('cart/{cart}', 'CartController@update');
	Route::post('cart/{cart}/shipTo', 'CartController@shipTo');
	Route::post('cart/{cart}/checkout', 'CheckoutController@checkout');
	Route::get("checkout-test",'CheckoutController@getTest');

	// Route::get('cart/{expressId?}', 'CartController@index')->name('cart.index');
	// Route::get('checkout/{slug}', 'CheckoutController@directCheckout');

	Route::post('register', 'AuthController@register');
	Route::post('update', 'AuthController@update');
	Route::post('login', 'AuthController@login');
	Route::post('logout', 'AuthController@logout');

	Route::post('mobile-checkout/{cart}', 'CheckoutController@mobileCheckout');
	Route::get('payments/{cartIds}/{delivery_cost}/{total}/{token}/{loginUserId}', 'CheckoutController@flutterPaymentsRedirect');

});


// Route::group(['middleware' => 'auth:api'], function() {
//     Route::get('articles', 'ArticleController@index');
//     Route::get('articles/{article}', 'ArticleController@show');
//     Route::post('articles', 'ArticleController@store');
//     Route::put('articles/{article}', 'ArticleController@update');
//     Route::delete('articles/{article}', 'ArticleController@delete');
// });